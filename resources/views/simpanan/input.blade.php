{{-- @extends('backpack::layout')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-body">
                    <h2><center>Laporan Simpanan</center></h2>
                    <form class="form" action="{{ route('simpanan.input') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group col-md-6">
                            <label>Tanggal Awal</label>
                            <input type="date" name="start" class="form-control" value="{{ app('request')->input('start') }}">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Tanggal Akhir</label>
                            <input type="date" name="end" class="form-control" value="{{ app('request')->input('end') }}">
                            <input type="hidden" name="pdf" value="1">
                        </div>
                        <div class="form-group col-md-6">
                            <button type="submit" class="btn col-md-3 btn-success">{{ "Submit" }}</button>
                        </div>
                    </form>
                    @if(app('request')->input('pdf'))
                    <form class="form" action="{{ route('simpanan.all.cetak') }}" method="post">
                        <div class="col-md-6">
                            {{ csrf_field() }}
                            <input type="hidden" name="start" class="form-control" value="{{ app('request')->input('start') }}">
                            <input type="hidden" name="end" class="form-control" value="{{ app('request')->input('end') }}">
                            <button type="submit" class="btn btn-info col-md-3">{{ "Cetak" }}</button>
                        </div>
                        <div class="col-md-12">
                            @include('pdf.simpanan_table')
                        </div>   
                    </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection --}}
