@extends('layouts.login')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-4 col-sm-12">
            <form class="card auth_form" method="POST" action="{{ route('login') }}">
                @csrf
                <div class="header">
                    <img class="logo" src="{{asset('vendor/assets/images/logobpr.png')}}" alt="">
                    <h5>Log in</h5>
                </div>
                <div class="body">
                    <div class="input-group mb-3">
                        <input id="email" type="email" class="form-control" {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="E-mail">
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="zmdi zmdi-account-circle"></i></span>
                        </div>
                        @if ($errors->has('email'))
                            <span class="alert-danger">
                                <small>{{ $errors->first('email') }}</small>
                            </span>
                        @endif
                    </div>
                    <div class="input-group mb-3">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">
                        <div class="input-group-append">                                
                            <span class="input-group-text"><i class="zmdi zmdi-lock"></i></span>
                        </div>  
                        @if ($errors->has('password'))
                            <span class="alert-danger">
                                <small>{{ $errors->first('password') }}</small>
                            </span>
                        @endif                          
                    </div>
                    <div class="checkbox">
                        <input id="remember_me" type="checkbox">
                        <label for="remember_me">Remember Me</label>
                    </div>
                    <div class="form-group row mb-0">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary btn-block waves-effect waves-light">
                                {{ __('Login') }}
                            </button>

                            {{-- <a class="btn btn-link" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a> --}}
                        </div>
                    </div>
                </div>
            </form>
            <div class="copyright text-center">
                &copy;
                <script>document.write(new Date().getFullYear())</script>,
                <span> Putu Elga Arya Putra
            </div>
        </div>
        <div class="col-lg-8 col-sm-12">
            <div class="card">
                <img src="{{asset('vendor/assets/images/signup.svg')}}" alt="Sign In"/>
            </div>
        </div>
    </div>
</div>
@endsection
