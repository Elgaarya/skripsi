@if (Auth::check())
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        @include('backpack::inc.sidebar_user_panel')

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          {{-- <li class="header">{{ trans('backpack::base.administration') }}</li> --}}
          <!-- ================================================ -->
          <!-- ==== Recommended place for admin menu items ==== -->
          <!-- ================================================ -->
          <li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-home"></i>{{ trans('backpack::base.dashboard') }}</a></li>

          {{-- jika user yang login memiliki role 'admin' --}}
          @if(Auth::user()->hasRole('admin'))

          {{-- link jika dia admin --}}
            <li class="treeview">
              <a href="#"><i class="fa fa-archive"></i>Master Data Anggota<i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                  {{-- <li><a href="{{ backpack_url('jenis_simpanan') }}"><i class="fa fa-database"></i>{{ "Jenis Simpanan" }}</a></li> --}}
                  <li><a href="{{ backpack_url('data_anggota') }}"><i class="fa fa-address-book"></i>{{ "Data Anggota" }}</a></li>
                  <li><a href="{{ route('crud.app/user.index') }}"><i class="fa fa-users"></i>{{ "Data Pengguna" }}</a></li>
              </ul>
            </li>
             <li class="treeview">

            <li class="treeview">
            <a href="#"><i class="fa fa-archive"></i>Master Data Transaksi<span class="badge pull-right">{{ App\pengajuan_pinjaman::where('status','pending')->count() }}</span><i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
                <li><a href="{{ backpack_url('jenis_pinjaman') }}"><i class="fa fa-database"></i>{{ "Jenis Pinjaman" }}</a></li>
                <li><a href="{{ backpack_url('pinjaman') }}"><i class="fa fa-credit-card"></i>{{ "Pinjaman" }}</a></li>
                <li><a href="{{ backpack_url('angsuran') }}"><i class="fa fa-dollar"></i>{{ "Angsuran" }}</a></li>
                <li>
                  <a href="{{ route('crud.app/pengajuan_pinjaman.index') }}">
                    <i class="fa fa-credit-card-alt"></i>
                    {{ "Pengajuan Pinjaman" }}
                    @if(Auth::user()->hasRole('admin'))
                      <span class="badge pull-right">{{ App\pengajuan_pinjaman::where('status','pending')->count() }}</span>
                    @endif
                </a>
                </li>
                
                 {{-- <li><a href="{{ backpack_url('jenis_simpanan') }}"><i class="fa fa-database"></i>{{ "Jenis Simpanan" }}</a></li> --}}
            </ul>
            </li>

            {{-- <li class="treeview">
            <a href="#"><i class="fa fa-book"></i>Simpanan<i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
                 <li><a href="{{ backpack_url('simpanan_pokok') }}"><i class="fa fa-money"></i>{{ "Simpanan Pokok" }}</a></li>
                 <li><a href="{{ backpack_url('simpanan_wajib') }}"><i class="fa fa-money"></i>{{ "Simpanan Wajib" }}</a></li>
                <li><a href="{{ backpack_url('simpanan_sukarela') }}"><i class="fa fa-money"></i>{{ "Simpanan Sukarela" }}</a></li>
               
            </ul>
             <li><a href="{{ backpack_url('penarikan_simpanan') }}"><i class="fa fa-fax"></i>{{ "Penarikan Simpanan" }}</a></li> --}}

          @endif

         
          {{-- link jika manager --}}
          @if(Auth::user()->hasRole('manager'))
            <li><a href="{{ route('pinjaman.input') }}"><i class="fa fa-fax"></i>{{ "Laporan Pinjaman" }}</a></li>
            <li><a href="{{ route('report.get.angsuran') }}"><i class="fa fa-fax"></i>{{ "Laporan Angsuran" }}</a></li>
          @endif

          @if(Auth::user()->hasRole('pegawai'))
            <li><a href="{{ backpack_url('pengajuan_pinjaman') }}"><i class="fa fa-credit-card"></i>{{ "Pengajuan Pinjaman" }}</a></li>
            <li><a href="{{ backpack_url('pinjaman') }}"><i class="fa fa-credit-card"></i>{{ "Pinjaman" }}</a></li>
            <li><a href="{{ backpack_url('angsuran') }}"><i class="fa fa-dollar"></i><span>{{ "Angsuran" }}</span></a></li>
          @endif


        @if(Auth::user()->hasRole('anggota'))
            <li><a href="{{ backpack_url('pengajuan_pinjaman') }}"><i class="fa fa-credit-card"></i>{{ "Pengajuan Pinjaman" }}</a></li>
            <li><a href="{{ backpack_url('pinjaman') }}"><i class="fa fa-credit-card"></i>{{ "Pinjaman" }}</a></li>
            <li><a href="{{ backpack_url('angsuran') }}"><i class="fa fa-dollar"></i><span>{{ "Angsuran" }}</span></a></li>
          @endif

          @if(Auth::user()->hasRole('admin'))
            <li class="treeview">
              <a href="#"><i class="fa fa-fax"></i>Laporan<i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
              {{-- <li><a href="{{ route('simpanan.input') }}"><i class="fa fa-fax"></i>{{ "Laporan Simpanan" }}</a></li> --}}
              <li><a href="{{ route('pinjaman.input') }}"><i class="fa fa-fax"></i>{{ "Laporan Pinjaman" }}</a></li>
              <li><a href="{{ route('report.get.angsuran') }}"><i class="fa fa-fax"></i>{{ "Laporan Angsuran" }}</a></li>
            </ul>
          </li>
          @endif
        </ul>
      </section>
    </aside>
@endif
