<div class="navbar-brand">
    <button class="btn-menu ls-toggle-btn" type="button"><i class="zmdi zmdi-menu"></i></button>
    <a href="index.html"><img src="{{asset('vendor/assets/images/logobpr.png')}}" width="25" alt="Aero"><span class="m-l-10">Dashboard</span></a>
</div>
<div class="menu">
    <ul class="list">
        <li>
            <div class="user-info">
                <a class="image" href="{{ route('backpack.account.info') }}"><img src="{{ backpack_avatar_url(Auth::user()) }}" alt="User"></a>
                <div class="detail">
                    <span>{{Auth::user()->data_anggota->nama}}</span>
                    <small>{{Auth::user()->data_anggota->bagian}}</small>                        
                </div>
            </div>
        </li>
        @if (Auth::user()->roleId == 1)
        <li class="active open"><a href="{{ backpack_url('dashboard') }}"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a></li>
        <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-account"></i><span>Master Anggota</span></a>
            <ul class="ml-menu">
                <li><a href="{{ backpack_url('data_anggota') }}">Data Anggota</a></li>
                <li><a href="{{ route('crud.app/user.index') }}">Data Pengguna</a></li>                
            </ul>
        </li>
        <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-money"></i><span>Master Transaksi</span></a>
            <ul class="ml-menu">
                <li><a href="{{ backpack_url('jenis_pinjaman') }}">Jenis Pinjaman</a></li>
                <li><a href="{{ backpack_url('pinjaman') }}">Pinjaman</a></li>
                <li><a href="{{ backpack_url('angsuran') }}">Angsuran</a></li>
                <li><a href="{{ route('crud.app/pengajuan_pinjaman.index') }}"><i class="fa fa-credit-card-alt"></i>
                    {{ "Pengajuan Pinjaman" }}
                    @if(Auth::user()->hasRole('admin'))
                      <span class="badge pull-right">{{ App\pengajuan_pinjaman::where('status','pending')->count() }}</span>
                    @endif
                    </a>
                </li>
            </ul>
        </li>
        <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-assignment"></i><span>Laporan</span></a>
            <ul class="ml-menu">
                <li><a href="{{ route('pinjaman.input') }}">Laporan Pinjaman</a></li>
                <li><a href="{{ route('report.get.angsuran') }}">Laporan Angsuran</a></li>
            </ul>
        </li>
        <li class=""><a href="{{ route('backpack.auth.logout') }}"><i class="zmdi zmdi-power"></i><span>Logout</span></a></li>
        @endif
        @if (Auth::user()->roleId == 4)
            <li class="active open"><a href="{{ backpack_url('dashboard') }}"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a></li>
            <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-money"></i><span>Master Transaksi</span></a>
                <ul class="ml-menu">
                    <li><a href="{{ backpack_url('pinjaman') }}">Pinjaman</a></li>
                    <li><a href="{{ backpack_url('angsuran') }}">Angsuran</a></li>
                    <li><a href="{{ backpack_url('pengajuan_pinjaman') }}">Pengajuan Pinjaman</a></li>
                </ul>
            </li>
            <li class=""><a href="{{ route('backpack.auth.logout') }}"><i class="zmdi zmdi-power"></i><span>Logout</span></a></li>
        @endif
        @if (Auth::user()->roleId == 2)
            <li class="active open"><a href="{{ backpack_url('dashboard') }}"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a></li>
            <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-assignment"></i><span>Laporan</span></a>
                <ul class="ml-menu">
                    <li><a href="{{ route('pinjaman.input') }}">Laporan Pinjaman</a></li>
                    <li><a href="{{ route('report.get.angsuran') }}">Laporan Angsuran</a></li>
                </ul>
            </li>
            <li class=""><a href="{{ route('backpack.auth.logout') }}"><i class="zmdi zmdi-power"></i><span>Logout</span></a></li>
        @endif
        @if (Auth::user()->roleId == 3)
            <li class=""><a href="{{ backpack_url('dashboard') }}"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a></li>
            <li class=""><a href="{{ backpack_url('pinjaman') }}"><i class="zmdi zmdi-money"></i><span>Pinjaman</span></a></li>
            <li class=""><a href="{{ backpack_url('angsuran') }}"><i class="zmdi zmdi-book"></i><span>Angsuran</span></a></li>
            <li class=""><a href="{{ backpack_url('pengajuan_pinjaman') }}"><i class="zmdi zmdi-card"></i><span>Pengajuan Pinjaman</span></a></li>
            <li class=""><a href="{{ route('backpack.auth.logout') }}"><i class="zmdi zmdi-power"></i><span>Logout</span></a></li>
        @endif
        
        
        
    </ul>
</div>