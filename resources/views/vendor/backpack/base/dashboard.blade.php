@extends('backpack::layout')
@section('content')
	@if(Auth::user()->roleId == 3)
			@include('partials.anggota')
		@else
			@include('partials.admin')
	@endif
@endsection
