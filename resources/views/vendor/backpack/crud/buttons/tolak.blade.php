@if($entry->status != 'terima' and $entry->status != 'tolak')
<form action="{{route('pengajuan.tolak',$entry->id) }}" method="post">
	{{ csrf_field() }}
	{{ method_field('PUT') }}
	<button class="btn btn-primary center" type="submit">Tolak</button>
</form>
@endif