@if (Auth::user()->hasRole('admin'))
	Total piutang : <a class="btn btn-success">{{ "Rp. " . number_format(App\Models\Pinjaman::sum('total_piutang')) }}</a>
@endif