@if(!$entry->sudah_dibayar == true)
<form action="{{route('angsuran.bayar',$entry->id) }}" method="post">
	{{ csrf_field() }}
	{{ method_field('PUT') }}
	<button class="btn btn-primary btn-xs" type="submit">Sudah diBayar</button>
</form>
@endif