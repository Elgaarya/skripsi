@if($entry->status != 'terima' and $entry->status != 'tolak')
<form action="{{route('pengajuan.terima',$entry->id) }}" method="post">
	{{ csrf_field() }}
	{{ method_field('PUT') }}
	<br><button class="btn btn-primary pull-left" type="submit">Terima</button>
</form>
@endif
