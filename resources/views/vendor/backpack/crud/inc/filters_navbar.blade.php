
<div class="container-fluid">
  <div class="header">
    <h2><strong>Filter</strong></h2>
      @foreach ($crud->filters as $filter)
        @include($filter->view)
      @endforeach
      <li><a href="#" id="remove_filters_button"><i class="fa fa-eraser"></i> {{ trans('backpack::crud.remove_filters') }}</a></li>

</div>
  <!-- Collect the nav links, forms, and other content for toggling -->
</div><!-- /.container-fluid -->

@push('crud_list_scripts')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/URI.js/1.18.2/URI.min.js" type="text/javascript"></script>
    <script>
      function addOrUpdateUriParameter(uri, parameter, value) {
            var new_url = normalizeAmpersand(uri);

            new_url = URI(new_url).normalizeQuery();

            if (new_url.hasQuery(parameter)) {
              new_url.removeQuery(parameter);
            }

            if (value != '') {
              new_url = new_url.addQuery(parameter, value);
            }

        return new_url.toString();
      }

      function normalizeAmpersand(string) {
        return string.replace(/&amp;/g, "&").replace(/amp%3B/g, "");
      }

      // button to remove all filters
      jQuery(document).ready(function($) {
      	$("#remove_filters_button").click(function(e) {
      		e.preventDefault();

		    	// behaviour for ajax table
		    	var new_url = '{{ url($crud->route.'/search') }}';
		    	var ajax_table = $("#crudTable").DataTable();

  				// replace the datatables ajax url with new_url and reload it
  				ajax_table.ajax.url(new_url).load();

  				// clear all filters
  				$(".navbar-filters li[filter-name]").trigger('filter:clear');
      	})
      });
    </script>
@endpush