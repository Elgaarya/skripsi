@php
	$i = 1;
	$current = 0;
@endphp
<table class="table table-hover">
	<thead>
		<th>No</th>
		<th>Tanggal</th>
		<th>Nama Anggota</th>
		<th>Besar Pinjaman</th>
		<th>Total Piutang</th>
		<th>Total Cicilan</th>
		<th>Sisa Piutang</th>
	</thead>
	<tbody>
		@foreach ($object as $single)
		@if ($current != $single->pinjaman_id)
			<tr>
				<td>{{ $i }}</td>
				<td>{{ $single->tanggal_bayar }}</td>
				<td>{{ $single->data_anggota->nama }}</td>
				<td>{{ number_format($single->pinjaman->besar) }}</td>
				@php
					$piutang = $single->pinjaman->jumlah * $single->pinjaman->lama;
					$sisa = ($single->pinjaman->jumlah * $single->pinjaman->lama) - ($single->pinjaman->jumlah * $single->angsuran_ke);
					$cicilan = ($single->pinjaman->jumlah * $single->angsuran_ke)
				@endphp
				<td>{{ $single->tanggal_bayar == null ? '-' : number_format($piutang) }}</td>
				<td>{{ $single->tanggal_bayar == null ? '-' : number_format($cicilan) }}</td>
				<td>{{ $single->tanggal_bayar == null ? '-' : number_format($sisa) }}</td>
			</tr>
		@endif
		@php
			$i += 1;
			$current = $single->pinjaman_id;
		@endphp
		@endforeach
	</tbody>
</table>