@extends('backpack::layout')

@section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="box box-default">
                <div class="body box-body">
                    <h2><center>Laporan Angsuran</center></h2>
                    <form class="form" action="{{ route('report.post.angsuran') }}" method="post">
                        {{ csrf_field() }}
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tanggal Awal</label>
                                        <input type="date" name ="start" class="form-control" value="{{ app('request')->input('start')}}" placeholder="col-md-6">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tanggal Akhir</label>
                                        <input type="date" name="end" class="form-control" value="{{app('request')->input('end')}}" placeholder="col-md-6">
                                        <input type="hidden" name="pdf" value="1">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <button type="submit" class="btn col-md-3 btn-success">{{ "Submit" }}</button>
                            </div>
                        </div>
                    </form>
                    @if(app('request')->input('pdf'))
                    <form class="form" action="{{ route('report.pdf.angsuran') }}" method="post">
                        <div class="col-md-6">
                            {{ csrf_field() }}
                            <input type="hidden" name="start" class="form-control" value="{{ app('request')->input('start') }}">
                            <input type="hidden" name="end" class="form-control" value="{{ app('request')->input('end') }}">
                            <button type="submit" class="btn btn-info col-md-3">{{ "Cetak" }}</button>
                        </div>
                        <div class="col-md-12">
                            @include('reports.tables.angsuran')
                        </div>   
                        {!! $chart->render() !!}

                    </form>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection
