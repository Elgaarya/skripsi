<html>
<head>
<title>Angsuran Simpan Pinjam BPR Bank Sutra Mas</title>
<style type="text/css">
  body{
    font-size: 12px;
  }
  #page-wrap {
    width: 700px;
    margin: 0 auto;
  }
  .center-justified {
    text-align: justify;
    margin: 0 auto;
    width: 30em;
  }
  table.outline-table {
    border: 1px solid;
    border-spacing: 0;
  }
  tr.border-bottom td, td.border-bottom {
    border-bottom: 1px solid;
  }
  tr.border-top td, td.border-top {
    border-top: 1px solid;
  }
  tr.border-right td, td.border-right {
    border-right: 1px solid;
  }
  tr.border-right td:last-child {
    border-right: 0px;
  }
  tr.center td, td.center {
    text-align: center;
    vertical-align: text-top;
  }
  td.pad-left {
    padding-left: 5px;
  }
  td.pad-right {
    padding-right: 5px;
  }
  tr.right-center td, td.right-center {
    text-align: right;
    padding-right: 50px;
  }
  tr.right td, td.right {
    text-align: right;
  }
  .grey {
    background:grey;
  }
</style>
</head>
<body>
  <div id="page-wrap">
    <table width="100%">
      <tr class="center">
        <td colspan="4"><p><strong>ANGSURAN PINJAMAN BPR BANK SUTRA MAS</strong></p></td>
      </tr>
      <tr>
        <td><strong>No Anggota</strong></td>
        <td colspan="3">: {{ $pinjaman->data_anggota->no_anggota }}</td>
      </tr>
      <tr>
        <td><strong>Nama Anggota</strong></td>
      <td colspan="3">: {{ $pinjaman->data_anggota->nama }}</td>
      </tr>
      <tr>
        <td><strong>Bagian</strong></td>
        <td colspan="3">: {{ $pinjaman->data_anggota->bagian }}</td>
      </tr>
      <tr>
        <td><strong>Tanggal Pinjaman</strong></td>
        <td colspan="3">: {{ date('d F Y', strtotime($pinjaman->tanggal)) }}</td>
      </tr>
      <tr>
        <td><strong>Jenis Pinjaman</strong></td>
        <td colspan="3">: {{ $pinjaman->jenis_pinjaman->nama }}</td>
      </tr>
      <tr>
        <td><strong>Bunga Pinjaman</strong></td>
        <td colspan="3">: {{ $pinjaman->jenis_pinjaman->bunga }} %</td>
      </tr>
      <tr>
        <td><strong>Besar Pinjaman</strong></td>
        <td>: Rp. {{ number_format($pinjaman->besar) }}</td>
        <td><strong>Angsuran Pokok</strong></td>
        <td>: Rp. {{ number_format($pinjaman->angsuran) }}</td>
      </tr>
      <tr>
        <td><strong>Jangka Waktu</strong></td>
        <td>: {{ $pinjaman->lama }} bulan</td>
        <td><strong>Bunga</strong></td>
        <td>: Rp. {{ number_format($pinjaman->bunga) }}</td>
      </tr>
    </table>
    &nbsp;
    <table width="100%" class="outline-table">
      <tr class="center border-right border-bottom">
        <td><p>Angsuran ke</p></td>
        <td><p>Tanggal</p></td>
        <td><p>Jumlah Bayar</p></td>
        <td><p>Total Bayar</p></td>
        <td><p>Sisa piutang</p></td>
        <td><p>Status bayar</p></td>
      </tr>
      @foreach ($pinjaman->angsuran()->get() as $angsuran)
      @php
        $sisa = ($angsuran->pinjaman->jumlah * $angsuran->pinjaman->lama) - ($angsuran->pinjaman->jumlah * $angsuran->angsuran_ke);
      @endphp
        <tr class="border-right border-bottom">
          <td class="center">{{ $angsuran->angsuran_ke }}</td>
          <td class="center">{{ $angsuran->tanggal_bayar }}</td>
          <td class="right pad-right">{{ number_format($pinjaman->jumlah) }}</td>
          <td class="right pad-right">{{ $angsuran->tanggal_bayar == null ? null : number_format($angsuran->angsuran_ke * $pinjaman->jumlah) }}</td>
          <td class="right pad-right">{{ $angsuran->tanggal_bayar == null ? null : number_format($sisa) }}</td>
          <td class="pad-left">{{ $angsuran->tanggal_bayar == null ? 'Belum' : 'Sudah' }}</td>
        </tr>
      @endforeach
    </table>
  </div>
</body>
</html>