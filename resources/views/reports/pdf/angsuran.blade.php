<html>
<head>
<title>Laporan Angsuran Anggota</title>
<style type="text/css">
  body{
    font-size: 12px;
  }
  #page-wrap {
    width: 700px;
    margin: 0 auto;
  }
  .center-justified {
    text-align: justify;
    margin: 0 auto;
    width: 30em;
  }
  table.outline-table {
    border: 1px solid;
    border-spacing: 0;
  }
  tr.border-bottom td, td.border-bottom {
    border-bottom: 1px solid;
  }
  tr.border-top td, td.border-top {
    border-top: 1px solid;
  }
  tr.border-right td, td.border-right {
    border-right: 1px solid;
  }
  tr.border-right td:last-child {
    border-right: 0px;
  }
  tr.center td, td.center {
    text-align: center;
    vertical-align: text-top;
  }
  td.pad-left {
    padding-left: 5px;
  }
  td.pad-right {
    padding-right: 5px;
  }
  tr.right-center td, td.right-center {
    text-align: right;
    padding-right: 50px;
  }
  tr.right td, td.right {
    text-align: right;
  }
  .grey {
    background:grey;
  }
</style>
</head>
<body>
  <div id="page-wrap">
    <table width="100%">
      <tr class="center">
        <td><strong>Laporan Angsuran Anggota</strong></td>
      </tr>
      <tr class="center">
        <td><strong>BPR BANK SUTRA MAS</strong></td>
      </tr>
      <tr class="center">
        <td><strong>Tahun {{ date('Y') }}</strong></td>
      </tr>
    </table>
    &nbsp;
    <table width="100%" class="outline-table">
      <thead>
        <tr class="border-right border-bottom center">
          <td><strong>No</strong></td>
          <td><strong>Tanggal</strong></td>
          <td><strong>Nama Anggota</strong></td>
          <td><strong>Besar Pinjaman</strong></td>
          <td><strong>Total Piutang</strong></td>
          <td><strong>Total Cicilan</strong></td>
          <td><strong>Sisa Piutang</strong></td>
        </tr>
      </thead>
      <tbody>
        @php
          $i = 1;
          $current = 0;
          $sum['cicilan'] = 0;
          $sum['piutang'] = 0;
          foreach ($pinjaman as $bunga) {
            $sum['piutang'] += $bunga->lama * $bunga->bunga;
          }
          $sum['piutang'] += $pinjaman->sum('besar');
          $sum['sisa'] = $pinjaman->sum('total_piutang');
        @endphp
        @foreach ($object as $single)
          @if ($current != $single->pinjaman_id)
            <tr class="border-right border-bottom">
              <td class="center">{{ $i }}</td>
              <td class="center">{{ $single->tanggal_bayar == null ? '-' : $single->tanggal_bayar}}</td>
              <td class="pad-left">{{ $single->data_anggota->nama }}</td>
              <td class="right pad-right">{{ number_format($single->pinjaman->besar) }}</td>
              @php
                $piutang = $single->pinjaman->jumlah * $single->pinjaman->lama;
                $sisa = ($single->pinjaman->jumlah * $single->pinjaman->lama) - ($single->pinjaman->jumlah * $single->angsuran_ke);
                $cicilan = ($single->pinjaman->jumlah * $single->angsuran_ke);
                if($single->tanggal_bayar != null){
                  $sum['cicilan'] += $single->pinjaman->jumlah;
                }
              @endphp
              <td class="right pad-right">{{ $single->tanggal_bayar == null ? '-' : number_format($piutang) }}</td>
              <td class="right pad-right">{{ $single->tanggal_bayar == null ? '-' : number_format($cicilan) }}</td>
              <td class="right pad-right">{{ $single->tanggal_bayar == null ? '-' : number_format($sisa) }}</td>
            </tr>
          @endif
        @php
          $i += 1;
          $current = $single->pinjaman_id;
        @endphp
        @endforeach
        <tr class="border-right">
          <td colspan="3" class="center">Total</td>
          <td class="right pad-right">{{ number_format($pinjaman->sum('besar')) }}</td>
          <td class="right pad-right">{{ number_format($sum['piutang']) }}</td>
          <td class="right pad-right">{{ number_format($sum['cicilan']) }}</td>
          <td class="right pad-right">{{ number_format($sum['sisa']) }}</td>
        </tr>
      </tbody>
    </table>
    &nbsp;
    <table width="100%">
      <tr>
        <td colspan="3" class="right">Tabanan Bali, {{ date('d F Y') }}</td>
      </tr>
      <tr><td colspan="3">&nbsp;</td></tr>
      <tr class="center"><td colspan="3">Pengurus</td></tr>
      <tr class="center">
        <td>Ketua</td>
        <td>&nbsp;</td>
        <td>Bendahara</td>
      </tr>
      <tr><td height="70px" colspan="3">&nbsp;</td></tr>
      <tr class="center">
        <td>(.................................)</td>
        <td>&nbsp;</td>
        <td>(.................................)</td>
      </tr>
    </table>
  </div>
</body>
</html>