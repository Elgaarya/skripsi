@extends('backpack::layout')

@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="box box-default">
            <div class="body box-body">
                <div class="card">
                  <div class="card-header">
                    <form action="" method="GET" id="form-filter">
                      <div class="row">
                        <div class="col-md-4">
                          @if(Auth::user()->roleId == 1)
                          <select name="status" class="form-control status">
                            <option value="">Pilih</option>
                            <option value="1" {{ ($_GET)?($_GET['status'] == 1)?'selected':'':'' }}>Sudah Bayar</option>
                            <option value="0" {{ ($_GET)?($_GET['status'] == 0)?'selected':'':'' }}>Belum Bayar</option>
                          </select>
                          @endif
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="card-body">
                    <table class="table table-bordered table-striped dataTable">
                      <thead>
                        <tr>
                          <th>Angsuran Ke</th>
                          <th>Status Bayar</th>
                          <th>Sisa Piutang</th>
                          <th>Tanggal Bayar</th>
                          <th>Besar Pinjaman</th>
                          <th>Pemilik akun</th>
                          <th>Jumlah Bayar</th>
                          <th>Telah Dibayar</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($angsurans as $row)
                          <tr>
                            <td>{{ $row->angsuran_ke }}</td>
                            <td>{{ ($row->sudah_dibayar == 1)?'Sudah Dibayar':'Belum Dibayar' }}</td>
                            <td>{{ $row->sisa_piutang }}</td>
                            <td>{{ $row->tanggal_bayar }}</td>
                            <td>{{ $row->pinjaman->besar }}</td>
                            <td>{{ $row->data_anggota->nama }}</td>
                            <td>{{ $row->jumlah_bayar }}</td>
                            <td>{{ $row->angsuran_ke * $row->jumlah_bayar }}</td>
                            <td>
                              @if($row->sudah_dibayar == 0 && Auth::user()->roleId == 1)
                              <form action="{{ route('angsuran.bayar', ['id' => $row->id]) }}" method="post">
                                @csrf
                                @method('PUT')
                                <button class="btn btn-primary btn-xs" type="submit">Sudah diBayar</button>
                              </form>
                              @endif
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>                    
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
  $(document).ready(function(){
    $('.dataTable').DataTable();

    // form filter
    $('#form-filter').on('change', function(){
      $('#form-filter').submit();
    });
  });
</script>
@endsection
