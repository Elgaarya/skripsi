<html>
<head>
<title>Bukti Pembayaran</title>
<style type="text/css">
  #page-wrap {
    width: 700px;
    margin: 0 auto;
  }
  .center-justified {
    text-align: justify;
    margin: 0 auto;
    width: 30em;
  }
  table.outline-table {
    border: 1px solid;
    border-spacing: 0;
  }
  tr.border-bottom td, td.border-bottom {
    border-bottom: 1px solid;
  }
  tr.border-top td, td.border-top {
    border-top: 1px solid;
  }
  tr.border-right td, td.border-right {
    border-right: 1px solid;
  }
  tr.border-right td:last-child {
    border-right: 0px;
  }
  tr.center td, td.center {
    text-align: center;
    vertical-align: text-top;
  }
  td.pad-left {
    padding-left: 5px;
  }
  td.pad-right {
    padding-right: 5px;
  }
  tr.right-center td, td.right-center {
    text-align: right;
    padding-right: 50px;
  }
  tr.right td, td.right {
    text-align: right;
  }
  .grey {
    background:grey;
  }
</style>
</head>
<body>
  <div id="page-wrap">
    <hr>
    <table width="30%" class="outline-table center right" style="position: absolute; right: 10">
      <tr>
        <td class="center">
          <small>BUKTI PEMBAYARAN</small>
        </td>
      </tr>
    </table>
    <table width="100%" style="margin-top: 20px; padding-left: 70px">
      <tr><td><small>BPR BUNG SUTRA MAS</small></td></tr>
    </table>
    &nbsp;
    <table width="100%">
      <tr>
        <td width="20%"><small>Telah terima dari </small></td>
        <td><small>: Bendahara BPR BUNGA SUTRA MAS</small></td>
      </tr>
      <tr>
        <td width="20%"><small>Uang sejumlah </small></td>
        <td><small>: Rp......................................................................................................................</small></td>
      </tr>
      <tr>
        <td width="20%"><small>&nbsp;</small></td>
        <td><small> : (.........................................................................................................................)</small></td>
      </tr>
    </table>

    <table width="100%">
      <tr>
        <td width="20%"><small>Untuk Pembayaran</small></td>
        <td colspan="2"><small>: Pengajuan pembiayaan</small></td>
      </tr>
      <tr>
        <td width="20%">&nbsp;</td>
        <td width="10%">&nbsp;</td>
        <td><small>1. Simpan Pinjam</small></td>
      </tr>
      <tr>
        <td width="20%">&nbsp;</td>
        <td width="10%">&nbsp;</td>
        <td><small>2. Emergency</small></td>
      </tr>
      <tr>
        <td width="20%">&nbsp;</td>
        <td width="10%">&nbsp;</td>
        <td><small>3. Alat Rumah Tangga</small></td>
      </tr>
      <tr>
        <td colspan="3">&nbsp;</td>
      </tr>
      <tr class="right">
        <td colspan="3"><small>Tabanan Bali, {{ date('d F Y') }}</small></td>
      </tr>
    </table>
    &nbsp;
    <table width="100%">
      <tr class="center">
        <td width="50%"><small>Yang membayarkan</small></td>
        <td width="50%"><small>Penerima</small></td>
      </tr>
      <tr class="center">
        <td width="50%"><small>Bendahara BPR BUNGA SUTRA MAS</small></td>
        <td width="50%">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" height="100px">&nbsp;</td>
      </tr>
      <tr class="center">
        <td width="50%">(....................................................)</td>
        <td width="50%">(....................................................)</td>
      </tr>
    </table>
    <p><sup>*)</sup> Tandai sesuai pengajuan</p>
    <hr>
  </div>
   <div id="page-wrap">
     <hr>
    <table width="30%" class="outline-table center right" style="position: absolute; right: 10">
      <tr>
        <td class="center">
          <small>BUKTI PEMBAYARAN</small>
        </td>
      </tr>
    </table>
    <table width="100%" style="margin-top: 20px; padding-left: 70px">
      <tr><td><small>BPR BUNGA SUTRA MAS</small></td></tr>
    </table>
    &nbsp;
    <table width="100%">
      <tr>
        <td width="20%"><small>Telah terima dari </small></td>
        <td><small>: Bendahara BPR BUNGA SUTRA MAS</small></td>
      </tr>
      <tr>
        <td width="20%"><small>Uang sejumlah </small></td>
        <td><small>: Rp......................................................................................................................</small></td>
      </tr>
      <tr>
        <td width="20%"><small>&nbsp;</small></td>
        <td><small> : (.........................................................................................................................)</small></td>
      </tr>
    </table>

    <table width="100%">
      <tr>
        <td width="20%"><small>Untuk Pembayaran</small></td>
        <td colspan="2"><small>: Pengajuan pembiayaan</small></td>
      </tr>
      <tr>
        <td width="20%">&nbsp;</td>
        <td width="10%">&nbsp;</td>
        <td><small>1. Simpan Pinjam</small></td>
      </tr>
      <tr>
        <td width="20%">&nbsp;</td>
        <td width="10%">&nbsp;</td>
        <td><small>2. Emergency</small></td>
      </tr>
      <tr>
        <td width="20%">&nbsp;</td>
        <td width="10%">&nbsp;</td>
        <td><small>3. Alat Rumah Tangga</small></td>
      </tr>
      <tr>
        <td colspan="3">&nbsp;</td>
      </tr>
      <tr class="right">
        <td colspan="3"><small>Tabanan Bali, {{ date('d F Y') }}</small></td>
      </tr>
    </table>
    &nbsp;
    <table width="100%">
      <tr class="center">
        <td width="50%"><small>Yang membayarkan</small></td>
        <td width="50%"><small>Penerima</small></td>
      </tr>
      <tr class="center">
        <td width="50%"><small>Bendahara BPR BUNGA SUTRA MAS</small></td>
        <td width="50%">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" height="100px">&nbsp;</td>
      </tr>
      <tr class="center">
        <td width="50%">(....................................................)</td>
        <td width="50%">(....................................................)</td>
      </tr>
    </table>
    <p><sup>*)</sup> Tandai sesuai pengajuan</p>
    <hr>
</body>
</html>