<table class="table table-hover">
	<thead>
		<th>No Anggota</th>
		<th>Nama Anggota</th>
		<th>Simpanan Pokok (Rp)</th>
		<th>Simpanan Wajib (Rp)</th>
		<th>Simpanan Sukarela (Rp)</th>
		<th>Jumlah</th>
	</thead>
	<tbody>
		@foreach ($anggota as $user)
			<tr>
				<td>{{ $user->no_anggota }}</td>
				<td>{{ $user->nama }}</td>
				@php
					$start = Carbon\Carbon::parse(app('request')->input('start'));
					$end = Carbon\Carbon::parse(app('request')->input('end'))->addDay()->subMinute();
					$all = $user->simpananPokok->sum('jumlah')+$user->simpananWajib()->whereBetween('created_at', [$start, $end])->sum('jumlah')+$user->simpananSukarela()->whereBetween('created_at', [$start, $end])->sum('jumlah');
				@endphp
				<td>{{ number_format($user->simpananPokok->sum('jumlah')) }}</td>
				<td>{{ number_format($user->simpananWajib()->whereBetween('created_at', [$start, $end])->sum('jumlah')) }}</td>
				<td>{{ number_format($user->simpananSukarela()->whereBetween('created_at', [$start, $end])->sum('jumlah')) }}</td>
				<td>{{ number_format($all) }}</td>
			</tr>
		@endforeach
	</tbody>
</table>