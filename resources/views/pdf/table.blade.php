<table class="table table-hover">
	<thead>
		<th>No Anggota</th>
		<th>Nama Anggota</th>
		<th>Jenis Pinjaman</th>
		<th>Besar (Rp)</th>
		<th>Angsuran (Rp)</th>
		<th>Bunga (Rp)</th>
		<th>Total Angsuran (Rp)</th>
	</thead>
	<tbody>
		@foreach ($p as $q)
			<tr>
				<td>{{ $q->data_anggota->no_anggota }}</td>
				<td>{{ $q->data_anggota->nama }}</td>
				<td>{{ $q->jenis_pinjaman->nama }}</td>
				<td>{{ number_format($q->besar) }}</td>
				<td>{{ number_format($q->angsuran) }}</td>
				<td>{{ number_format($q->bunga) }}</td>
				<td>{{ number_format($q->jumlah) }}</td>
			</tr>
		@endforeach
	</tbody>
</table>