<html>
<head>
<title>Laporanm Simpanan Anggota</title>
<style type="text/css">
  body{
    font-size: 12px;
  }
  #page-wrap {
    width: 700px;
    margin: 0 auto;
  }
  .center-justified {
    text-align: justify;
    margin: 0 auto;
    width: 30em;
  }
  table.outline-table {
    border: 1px solid;
    border-spacing: 0;
  }
  tr.border-bottom td, td.border-bottom {
    border-bottom: 1px solid;
  }
  tr.border-top td, td.border-top {
    border-top: 1px solid;
  }
  tr.border-right td, td.border-right {
    border-right: 1px solid;
  }
  tr.border-right td:last-child {
    border-right: 0px;
  }
  tr.center td, td.center {
    text-align: center;
    vertical-align: text-top;
  }
  td.pad-left {
    padding-left: 5px;
  }
  td.pad-right {
    padding-right: 5px;
  }
  tr.right-center td, td.right-center {
    text-align: right;
    padding-right: 50px;
  }
  tr.right td, td.right {
    text-align: right;
  }
  .grey {
    background:grey;
  }
</style>
</head>
<body>
  <div id="page-wrap">
    <table width="100%">
      <tr class="center">
        <td><strong>Laporan Simpanan Anggota</strong></td>
      </tr>
      <tr class="center">
        <td><strong>Koperasi Urania PTBBN - Batan</strong></td>
      </tr>
      <tr class="center">
        <td><strong>Tahun {{ date('Y') }}</strong></td>
      </tr>
    </table>
    &nbsp;
    <table width="100%" class="outline-table">
      <thead>
        <tr class="border-right border-bottom center">
          <td>No Anggota</td>
          <td>Nama Anggota</td>
          <td>Simpanan Pokok (Rp)</td>
          <td>Simpanan Wajib (Rp)</td>
          <td>Simpanan Sukarela (Rp)</td>
          <td>Jumlah</td>
        </tr>
        @foreach ($anggota as $user)
        @php
          $start = Carbon\Carbon::parse(app('request')->input('start'));
          $end = Carbon\Carbon::parse(app('request')->input('end'))->addDay()->subMinute();
          $all = $user->simpananPokok->sum('jumlah')+$user->simpananWajib()->whereBetween('created_at', [$start, $end])->sum('jumlah')+$user->simpananSukarela()->whereBetween('created_at', [$start, $end])->sum('jumlah');
          $sum_all += $all;
        @endphp
        <tr class="border-right">
          <td>{{ $user->no_anggota }}</td>
          <td>{{ $user->nama }}</td>
          <td class="right">{{ number_format($user->simpananPokok->sum('jumlah')) }}</td>
          <td class="right">{{ number_format($user->simpananWajib()->whereBetween('created_at', [$start, $end])->sum('jumlah')) }}</td>
          <td class="right">{{ number_format($user->simpananSukarela()->whereBetween('created_at', [$start, $end])->sum('jumlah')) }}</td>
          <td class="right">{{ number_format($all) }}</td>
        </tr>
        @endforeach
        <tr class="border-right border-top">
          <td colspan="2">Total Jumlah</td>
          <td class="right">{{ number_format($sp->sum('jumlah')) }}</td>
          <td class="right">{{ number_format($sw->sum('jumlah')) }}</td>
          <td class="right">{{ number_format($ss->sum('jumlah')) }}</td>
          <td class="right">{{ number_format($sum_all) }}</td>
        </tr>
      </thead>
    </table>
    &nbsp;
    <table width="100%">
      <tr>
        <td colspan="3" class="right">Tangerang Selatan, {{ date('d F Y') }}</td>
      </tr>
      <tr><td colspan="3">&nbsp;</td></tr>
      <tr class="center"><td colspan="3">Pengurus</td></tr>
      <tr class="center">
        <td>Ketua</td>
        <td>&nbsp;</td>
        <td>Bendahara</td>
      </tr>
      <tr><td height="70px" colspan="3">&nbsp;</td></tr>
      <tr class="center">
        <td>(.................................)</td>
        <td>&nbsp;</td>
        <td>(.................................)</td>
      </tr>
    </table>
  </div>
</body>
</html>