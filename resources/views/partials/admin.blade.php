@php
    $anggota = \App\Models\DataAnggota::all();
    $pinjaman = \App\Models\Pinjaman::all();
    $angsuran = \App\Models\Angsuran::whereNotNull('tanggal_bayar')->get();

    $chart = Charts::database($angsuran, 'bar', 'highcharts')
        ->title('Data Keuangan Perusahaan')
        ->elementLabel('Jumlah Nominal')
        ->responsive(false)
        ->dimensions(1000, 500)
        ->width(0)
        ->values([$pinjaman->sum('besar'),$angsuran->sum('jumlah_bayar')])
        ->labels(['Pinjaman', 'Terbayar']);
@endphp

<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="card widget_2 big_icon traffic">
                <div class="body">
                    <h6>Anggota</h6>
                    <h2>{{ $anggota->count() }} <small class="info">Orang</small></h2>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="card widget_2 big_icon sales">
                <div class="body">
                    <h6>Jumlah Pinjaman</h6>
                    <h2>Rp. {{ number_format($pinjaman->sum('besar')) }} <small class="info">Rupiah</small></h2>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="card widget_2 big_icon sales">
                <div class="body">
                    <h6>Jumlah Terbayar</h6>
                    <h2>Rp. {{ number_format($angsuran->sum('jumlah_bayar')) }} <small class="info">Rupiah</small></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    {!! $chart->render() !!}
                </div>
            </div>
        </div>
    </div>
</div>