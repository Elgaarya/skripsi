<div class="row">
    <div class="col-md-12">
      <div class="box box-default">
          <div class="box-body">
            <p class="lead">Welcome {{ Auth::user()->data_anggota->nama }}</p>
            <p>Selamat Datang Di Sistem Informai Peminjaman Modal</p>
            <p>Anda login menggunakan akun <strong>{{ Auth::user()->email }}</strong></p>
          </div>
      </div>
    </div>

    <div class="col-md-4">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-address-book"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Jumlah Anggota</span>
          @php
            $anggota = App\Models\DataAnggota::all()->count();
          @endphp
          <span class="info-box-number">{{ number_format($anggota) }}</span>
        </div>
      </div>
    </div>

    <div class="col-md-4">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-credit-card-alt"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Pengajuan Pinjaman</span>
          @php
            $pengajuan = App\pengajuan_pinjaman::where('status','pending')->count();
          @endphp
          <span class="info-box-number">{{ number_format($pengajuan) }}</span>
        </div>
      </div>
    </div>

    <div class="col-md-4">
      <div class="info-box">
        <span class="info-box-icon bg-red"><i class="fa fa-credit-card"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Total Pinjaman</span>
          @php
            $pinjaman = "Rp. " . number_format(App\Models\Pinjaman::sum('total_piutang'));
          @endphp
          <span class="info-box-number">{{ $pinjaman }}</span>
        </div>
      </div>
    </div>
</div>