<div class="row">
    <div class="col-md-12">
      <div class="box box-default">
          <div class="box-body">
            <p class="lead">Welcome {{ Auth::user()->data_anggota->nama }}</p>
            <p>Selamat Datang di Sistem Informasi Peminjaman Modal!</p>
            <p>Anda login menggunakan akun <strong>{{ Auth::user()->email }}</strong></p>
          </div>
      </div>
    </div>
    {{-- <div class="col-md-12">
      <div class="info-box">
        <div class="info-box-content">
          <div class="row">
            
          </div>
        </div>
      </div>
    </div> --}}
</div>