<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function welcome()
    {
    	// Meredirect langsung ke halaman Dashboard
    	return redirect()->route('backpack');
    }
}
