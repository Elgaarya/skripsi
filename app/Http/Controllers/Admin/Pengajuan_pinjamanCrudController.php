<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\Pengajuan_pinjamanRequest as StoreRequest;
use App\Http\Requests\Pengajuan_pinjamanRequest as UpdateRequest;
use Auth;
use Alert;
use Carbon\Carbon;
class Pengajuan_pinjamanCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Pengajuan_pinjaman');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/pengajuan_pinjaman');
        $this->crud->setEntityNameStrings('pengajuan_pinjaman', 'pengajuan_pinjamen');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $this->crud->setFromDb();
        $this->crud->removeField('tanggal','update/create/both');

        $this->crud->addField([
            'label' => 'Tanggal Pinjaman', // Table column heading
            'type' => 'date',
            'name' => 'tanggal', // the column that contains the ID of that connected entity;\
            'value' => date('Y-m-d'),
            'attributes' => [
                'min' => date('Y-m-d')
            ]
        ]);

        $this->crud->addField([
            'label' => 'Besar', // Table column heading
            'type' => 'number',
            'name' => 'besar',
            'hint' => 'Masukkan Hanya Angka!'
         ]);

         $this->crud->addField([
            'label' => 'Lama (Bulan)', // Table column heading
            'type' => 'number',
            'name' => 'lama',
            'hint' => 'Masukkan Hanya Angka!'
         ]);


        $this->crud->addField(
            [   
                'name' => 'data_anggota_id',
                'type' => "hidden",
                'value' => Auth::user()->data_anggota->id
            ]
        );
        $this->crud->addField(
            [   
                'label' => 'Jenis Pinjaman',
                'type' => "select",
                'name' => 'jenis_pinjaman_id', // the db column for the foreign key
                'entity' => 'jenis_pinjaman', // the method that defines the relationship in your Model
                'attribute' => 'label_name', // foreign key attribute that is shown to user
                'model' => "App\Models\Jenis_pinjaman" // foreign key model
            ]
        );
        // $this->crud->addField($options, 'update/create/both');
        // $this->crud->addFields($array_of_arrays, 'update/create/both');
        // $this->crud->removeField('name', 'update/create/both')
     
        if (Auth::user()->Roles->find(3)) {
        $this->crud->removeField('keterangan');
        }
        $this->crud->removeField('status');
        
        // $this->crud->removeFields($array_of_names, 'update/create/both');

        // ------ CRUD COLUMNS
         $this->crud->addColumn([
           'label' => 'Nama', // Table column heading
           'type' => 'select',
           'name' => 'data_anggota_id', // the column that contains the ID of that connected entity;
           'entity' => 'data_anggota', // the method that defines the relationship in your Model
           'attribute' => 'nama', // foreign key attribute that is shown to user
           'model' => "App\Models\DataAnggota", // foreign key model
        ]);
        $this->crud->addColumn(
            [   
                'label' => 'Jenis Pinjaman',
                'type' => "select",
                'name' => 'jenis_pinjaman_id', // the db column for the foreign key
                'entity' => 'jenis_pinjaman', // the method that defines the relationship in your Model
                'attribute' => 'nama', // foreign key attribute that is shown to user
                'model' => "App\Models\Jenis_pinjaman" // foreign key model
            ]
        );
        
        
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']); // adjusts the properties of the passed in column (by name)
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);

        // ------ CRUD BUTTONS
        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
        // $this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function
        // add a button; possible types are: view, model_function
        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->addButtonFromView($stack, $name, $view, $position); // add a button whose HTML is in a view placed at resources\views\vendor\backpack\crud\buttons
        if (Auth::user()->hasRole('admin')) { // Jika admin
            $this->crud->addButtonFromView('line', 'terima', 'terima', 'end');
            $this->crud->addButtonFromView('line','tolak','tolak','end');
            $this->crud->addButtonFromView('line', 'cetak', 'cetak', 'end');
        }

        if (Auth::user()->hasRole('admin')) {
            $this->crud->denyAccess(['create']);
        }
        // if (!Auth::user()->hasRole('anggota')) {
        //     $this->crud->denyAccess(['create']);
        // }
        if (Auth::user()->hasRole('anggota')) 
            {
                $this->crud->addClause('where', 'data_anggota_id', '=', Auth::user()->data_anggota->id);
                $this->crud->denyAccess(['update']);
            }

        $this->crud->enableExportButtons();

        if (Auth::user()->hasRole('admin')) {
            $this->crud->removeField(['tanggal']);
            $this->crud->removeField(['besar']);
            $this->crud->removeField(['lama']);
            $this->crud->removeField(['nama']);
             $this->crud->removeField(
            [   
                'label' => 'Jenis Pinjaman',
                'type' => "select",
                'name' => 'jenis_pinjaman_id', // the db column for the foreign key
                'entity' => 'jenis_pinjaman', // the method that defines the relationship in your Model
                'attribute' => 'nama', // foreign key attribute that is shown to user
                'model' => "App\Models\Jenis_pinjaman" // foreign key model
            ]);
        }
    }

    public function store(StoreRequest $request)
    {

        if (Carbon::parse($request->tanggal)->isPast()) {
            
            return redirect()->back()->with('danger', 'Tanggal yang dimasukkan sudah terlewat');
        }
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return redirect()->route('crud.app/pengajuan_pinjaman.index')->with('success', 'Pinjaman Berhasil Diajukan.');
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
