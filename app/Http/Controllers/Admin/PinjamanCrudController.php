<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

use App\Http\Requests\PinjamanRequest as StoreRequest;
use App\Http\Requests\PinjamanRequest as UpdateRequest;
use App\Models\Jenis_pinjaman;
use App\Models\Angsuran;
use Auth;
use DB;
use App\Models\Pinjaman;
class PinjamanCrudController extends CrudController
{
    
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Pinjaman');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/pinjaman');
        $this->crud->setEntityNameStrings('pinjaman', 'pinjamen');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $this->crud->setFromDb();
        
        $this->crud->addField([
            'label' => 'Tanggal', // Table column heading
            'type' => 'date',
            'name' => 'tanggal', // the column that contains the ID of that connected entity;\
            'value' => date('Y-m-d'),
            'attributes' => [
                'min' => date('Y-m-d')
            ]
         ]);

        $this->crud->addField(
            [   
                'label' => 'Pemilik Akun',
                'type' => "select",
                'name' => 'data_anggota_id', // the db column for the foreign key
                'entity' => 'data_anggota', // the method that defines the relationship in your Model
                'attribute' => 'nama', // foreign key attribute that is shown to user
                'model' => "App\Models\DataAnggota" // foreign key model
            ]
        );
        $this->crud->addField(
            [
                'label' => 'Jenis Pinjaman',
                'type' => "select",
                'name' => 'jenis_pinjaman_id',
                'entity' => 'jenis_pinjaman',
                'attribute' => 'nama',
                'model' => "App\Models\Jenis_pinjaman"
            ]
        );
        // ------ CRUD FIELDS
        // $this->crud->addField($options, 'update/create/both');
        // $this->crud->addFields($array_of_arrays, 'update/create/both');
        // $this->crud->removeField('name', 'update/create/both');
        $this->crud->removeField('angsuran');
        $this->crud->removeField('bunga');
        $this->crud->removeField('jumlah');
        $this->crud->removeField('total_piutang');

        // $this->crud->removeFields($array_of_names, 'update/create/both');

            $this->crud->addField([
            'label' => 'Lama (Bulan)', // Table column heading
            'type' => 'number',
            'name' => 'lama', // the column that contains the ID of that connected entity;\
            'hint' => 'Tuliskan hanya angka',
         ]);
         $this->crud->addField([
            'label' => 'Besar', // Table column heading
            'type' => 'number',
            'name' => 'besar', // the column that contains the ID of that connected entity;\
         ]);

         $this->crud->addColumn([
            'label' => 'Besar', // Table column heading
            'type' => 'number',
            'name' => 'besar', // the column that contains the ID of that connected entity;\
            'attributes' => ["step" => "any"], // allow decimals
            'prefix'     => "Rp.",
         ]);

         $this->crud->addColumn([
            'label' => 'Angsuran', // Table column heading
            'type' => 'number',
            'name' => 'angsuran', // the column that contains the ID of that connected entity;\
            'attributes' => ["step" => "any"], // allow decimals
            'prefix'     => "Rp.",
            'suffix'    => '/Bulan',
         ]);

         $this->crud->addColumn([
            'label' => 'Bunga', // Table column heading
            'type' => 'number',
            'name' => 'bunga', // the column that contains the ID of that connected entity;\
            'attributes' => ["step" => "any"], // allow decimals
            'prefix'     => "Rp.",
            'suffix'    => '/Bulan',
         ]);

         $this->crud->addColumn([
            'label' => 'Jumlah', // Table column heading
            'type' => 'number',
            'name' => 'jumlah', // the column that contains the ID of that connected entity;\
            'attributes' => ["step" => "any"], // allow decimals
            'prefix'     => "Rp.",
            'suffix'    => '/Bulan',
         ]);

         $this->crud->addColumn([
            'label' => 'Total Piutang', // Table column heading
            'type' => 'number',
            'name' => 'total_piutang', // the column that contains the ID of that connected entity;\
            'attributes' => ["step" => "any"], // allow decimals
            'prefix'     => "Rp.",

         ]);
         $this->crud->addColumn([
            'label' => 'Lama', // Table column heading
            'type' => 'text',
            'name' => 'lama', // the column that contains the ID of that connected entity;\
            'hint' => 'Tuliskan hanya angka',
            'suffix'     => ' bulan',
            
         ]);
        // ------ CRUD COLUMNS
         $this->crud->addColumn([
           'label' => 'Pemilik Akun', // Table column heading
           'type' => 'select',
           'name' => 'data_anggota_id', // the column that contains the ID of that connected entity;
           'entity' => 'data_anggota', // the method that defines the relationship in your Model
           'attribute' => 'nama', // foreign key attribute that is shown to user
           'model' => "App\Models\DataAnggota", // foreign key model
        ]);
          $this->crud->addColumn(
            [
                'label' => 'Jenis Pinjaman',
                'type' => "select",
                'name' => 'jenis_pinjaman_id',
                'entity' => 'jenis_pinjaman',
                'attribute' => 'nama',
                'model' => "App\Models\Jenis_pinjaman",
            ]
        );
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
         
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']); // adjusts the properties of the passed in column (by name)
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);

        // ------ CRUD BUTTONS
        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
        // $this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function
        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        $this->crud->addButtonFromView('top', 'total_piutang', 'total_piutang', 'end');
        //INI CETAK ANGSURAN
        //$this->crud->addButtonFromView('line', 'cetak_angsuran', 'cetak_angsuran', 'end');
        //
        // $this->crud->removeButton($name);
        // $this->crud->removeButtonFromStack($name, $stack);
        // $this->crud->removeAllButtons();
        // $this->crud->removeAllButtonsFromStack('line');

        // ------ CRUD ACCESS
        // $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);
        // \$this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);
        if (Auth::user()->roleId == 3){
        $this->crud->denyAccess(['create', 'update', 'delete']);
        $this->crud->addClause('where', 'data_anggota_id', '=', Auth::user()->data_anggota->id);
        }
        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        // $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ REVISIONS
        // You also need to use \Venturecraft\Revisionable\RevisionableTrait;
        // Please check out: https://laravel-backpack.readme.io/docs/crud#revisions
        // $this->crud->allowAccess('revisions');

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though:
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        // $this->crud->enableAjaxTable();

        // ------ DATATABLE EXPORT BUTTONS
        // Show export to PDF, CSV, XLS and Print buttons on the table view.
        // Does not work well with AJAX datatables.
        //\ $this->crud->enableExportButtons();
         $this->crud->enableExportButtons();


        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '==', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->addClause('withoutGlobalScopes');
        // $this->crud->addClause('withoutGlobalScope', VisibleScope::class);
        // $this->crud->with(); // eager load relationships
        // $this->crud->orderBy();
        // $this->crud->groupBy();
        // $this->crud->limit();
    }

    public function store(StoreRequest $request)
    {
        $jp = Jenis_pinjaman::find($request->jenis_pinjaman_id);
        $angsuran = $request->besar/$request->lama; //12 = 84.000
        $bunga = ($jp->bunga/100)*$request->besar; // bunga dikalikan pokok = 1.000.000 * 5% = 50.000
        $jumlah = $angsuran+$bunga;
        $request->request->add([
            'angsuran' => $angsuran,
            'bunga' => $bunga,
            'jumlah' => $jumlah,
            'total_piutang' => $jumlah*$request->lama
        ]);
        $redirect_location = parent::storeCrud($request);
        for ($i=0; $i < $request->lama; $i++) { 
            $angsuran = new Angsuran;
            $angsuran->angsuran_ke = $i+1;
            $angsuran->jumlah_bayar = $jumlah;
            $angsuran->pinjaman_id = $this->crud->entry->id;
            $angsuran->data_anggota_id = $request->data_anggota_id;
            $angsuran->save();
        }
        return redirect()->route('crud.app/pinjaman.index')->with('success', 'Data pinjaman berhasil ditambahkan.');
    }

    public function update(UpdateRequest $request, $id)
    {
        $jp = Pinjaman::find($id);
        $angsuran = $request->besar/$request->lama; //12 = 84.000
        $bunga = ($jp->jenis_pinjaman->bunga/100)*$request->besar; // bunga dikalikan pokok = 1.000.000 * 5% = 50.000
        $jumlah = $angsuran+$bunga;
        $request->request->add([
            'angsuran' => $angsuran,
            'bunga' => $bunga,
            'jumlah' => $jumlah,
            'total_piutang' => $jumlah*$request->lama
        ]);
        // dd($jp->angsurans);
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        foreach ($jp->angsurans as $index => $angsuran) { 
            $angsuran->angsuran_ke = $index+1;
            $angsuran->jumlah_bayar = $jumlah;
            $angsuran->pinjaman_id = $this->crud->entry->id;
            $angsuran->data_anggota_id = $request->data_anggota_id;
            $angsuran->save();
        }
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
