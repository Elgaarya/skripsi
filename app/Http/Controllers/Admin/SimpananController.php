<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;
use Carbon\Carbon;
use App\Models\DataAnggota;
use App\Models\Simpanan_wajib;
use App\Models\Simpanan_sukarela;
use App\Models\SimpananPokok;
class SimpananController extends Controller
{
    public function postInput(Request $request)
    {
    	$start = Carbon::parse($request->start);
    	$end = Carbon::parse($request->end)->addDay()->subMinute();
        $anggota = DataAnggota::orderBy('nama')->get();
    	return view('simpanan.input', compact('anggota'));
    }
    public function input(Request $request)
    {
    	return view('simpanan.input');
    }
    public function cetak(Request $request)
    {
    	$start = Carbon::parse($request->start);
    	$end = Carbon::parse($request->end)->addDay()->subMinute();
        $anggota = DataAnggota::orderBy('nama')->get();
        $data['sw'] = Simpanan_wajib::whereBetween('created_at', [$start, $end])->get();
        $data['sp'] = SimpananPokok::all();
        $data['ss'] = Simpanan_sukarela::whereBetween('created_at', [$start, $end])->get();
        $data['anggota'] = $anggota;
    	$data['sum_all'] = 0;
    	$pdf = PDF::loadView('pdf.simpanan', $data);
    	return $pdf->stream('simpanan.pdf');
    }
}
