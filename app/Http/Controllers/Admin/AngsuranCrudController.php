<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\AngsuranRequest as StoreRequest;
use App\Http\Requests\AngsuranRequest as UpdateRequest;
use App\Models\DataAnggota;
use App\Models\Angsuran;
use Auth;
class AngsuranCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Angsuran');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/angsuran');
        $this->crud->setEntityNameStrings('angsuran', 'angsurans');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $this->crud->setFromDb();

        // ------ CRUD FIELDS
        $this->crud->addField(
            [
                'label' => 'Pemilik Akun',
                'type' => "select",
                'name' => 'data_anggota_id',
                'entity' => 'data_anggota',
                'attribute' => 'nama',
                'model' => "App\Models\DataAnggota"
            ]
        );
  
        
        // $this->crud->addField($options, 'update/create/both');
        // $this->crud->addFields($array_of_arrays, 'update/create/both');
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');

        // ------ CRUD COLUMNS
        $this->crud->addFilter([ // select2 filter
          'name' => 'status',
          'type' => 'select2',
          'label'=> 'Pemilik'
        ], function() {
            $array = [0=> 'Pilih'];
            foreach (DataAnggota::orderBy('id')->get() as $user) {
                $array = array_merge($array, [$user->id => $user->nama]);
            }
            return $array;
        }, function($value) {
            $this->crud->addClause('where', 'data_anggota_id', '=', $value);
        });
        $this->crud->addColumn(
            [
                'label' => 'Pemilik Akun',
                'type' => "select",
                'name' => 'data_anggota_id',
                'entity' => 'data_anggota',
                'attribute' => 'nama',
                'model' => "App\Models\DataAnggota"
            ]
        );
        $this->crud->addFilter([ // simple filter
          'type' => 'simple',
          'name' => 'sudah_dibayar',
          'label'=> 'Tampilkan hanya yang belum dibayar'
        ], 
        false, 
        function($value) { // if the filter is active
            $this->crud->addClause('where', 'sudah_dibayar', '=', $value); // apply the "active" eloquent scope 
        } );
        $this->crud->addColumn(
            [
                'label' => 'Status Bayar',
                'type' => "select_from_array",
                'name' => 'sudah_dibayar',
                'options' => [
                    0 => 'Belum Dibayar',
                    1=> 'Sudah Dibayar'
                ]
            ]
        );

        $this->crud->addColumn(
            [
                'label' => 'Jumlah Bayar',
                'type' => 'number',
                'name' => 'jumlah_bayar',
                'attributes' => ["step" => "any"], // allow decimals
                'prefix'     => "Rp.",
            ]
        );
        $this->crud->addColumn(
            [
                'label' => 'Telah Dibayar',
                'type' => "telah_bayar",
                'name' => 'telah_bayar',

            ]
        );
        $this->crud->addColumn(
            [
                'label' => 'Besar Pinjaman',
                'type' => "select",
                'name' => 'pinjaman_id',
                'entity' => 'pinjaman',
                'attribute' => 'besar',
                'attributes' => ["step" => "any"], // allow decimals
                'prefix'     => "Rp.",
                'model' => "App\Models\Pinjaman"
            ]
        );

        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']); // adjusts the properties of the passed in column (by name)
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);

        // ------ CRUD BUTTONS
        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
        // $this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function
        if (Auth::user()->hasRole('admin')){
        $this->crud->addButtonfromView('line', 'bayar', 'bayar', 'end');
        } 

        if (Auth::user()->hasRole('anggota')){
         $this->crud->addClause('where', 'data_anggota_id', '=', Auth::user()->data_anggota->id);
         }// add a button; possible types are: view, model_function
        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->addButtonFromView($stack, $name, $view, $position); // add a button whose HTML is in a view placed at resources\views\vendor\backpack\crud\buttons
        // $this->crud->removeButton($name);
        // $this->crud->removeButtonFromStack($name, $stack);
        // $this->crud->removeAllButtons();
        // $this->crud->removeAllButtonsFromStack('line');

        // ------ CRUD ACCESS
        // $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);
        // $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);
        $this->crud->denyAccess(['create','update','delete']);


        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right data_anggotas: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        // $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right data_anggotas: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ REVISIONS
        // You also need to use \Venturecraft\Revisionable\RevisionableTrait;
        // Please check out: https://laravel-backpack.readme.io/docs/crud#revisions
        // $this->crud->allowAccess('revisions');

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though:
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        // $this->crud->enableAjaxTable();

        // ------ DATATABLE EXPORT BUTTONS
        // Show export to PDF, CSV, XLS and Print buttons on the table view.
        // Does not work well with AJAX datatables.
        // $this->crud->enableExportButtons();
        $this->crud->enableExportButtons();

        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '==', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->addClause('withoutGlobalScopes');
        // $this->crud->addClause('withoutGlobalScope', VisibleScope::class);
        // $this->crud->with(); // eager load relationships
        // $this->crud->orderBy();
        // $this->crud->groupBy();
        // $this->crud->limit();
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
