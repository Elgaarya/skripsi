<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;
use App\Models\Angsuran;
use App\Models\Pinjaman;
use Carbon\Carbon;
use Charts;

class ReportController extends Controller
{
    public function __construct()
    {
		$this->middleware(['auth']);
    }

    public function angsuran()
    {
    	return view('reports.angsuran');
    }

    public function postAngsuran(Request $request)
    {
        $start = Carbon::parse($request->start);
        $end = Carbon::parse($request->end)->addDay()->subMinute();
        $object = Angsuran::whereBetween('created_at', [$start, $end])->whereNotNull('tanggal_bayar')->orderBy('pinjaman_id')->orderBy('updated_at', 'desc')->get();
        // dd($object);
        $chart = Charts::database(Angsuran::all(), 'bar', 'highcharts')
        ->title('Jumlah Pembayaran Angsuran Tiap Bulan')
        ->elementLabel('Jumlah Pembayaran')
        ->responsive(false)
        ->dimensions(1000, 500)
        ->width(0)
        ->values([$object->sum('jumlah_bayar'),$object->sum('sisa_piutang')])
        ->labels(['Sudah di Bayar', 'Belum Di Bayar']);
    	return view('reports.angsuran', compact('object', 'chart'));
    }

    public function submitAngsuran(Request $request)
    {
        $start = Carbon::parse($request->start);
        $end = Carbon::parse($request->end)->addDay()->subMinute();
        $data['object'] = Angsuran::whereBetween('created_at', [$start, $end])->whereNotNull('tanggal_bayar')->orderBy('pinjaman_id')->orderBy('updated_at', 'desc')->get();
        $data['pinjaman'] = Pinjaman::whereBetween('tanggal', [$start, $end])->get();
    	$pdf = PDF::loadView('reports.pdf.angsuran', $data);
    	return $pdf->stream('angsuran.pdf');
    }

    public function pinjaman($id)
    {
        $data['pinjaman'] = Pinjaman::find($id);
        $pdf = PDF::loadView('reports.pdf.pinjaman', $data);
        return $pdf->stream('pinjaman.pdf');
    }
}
