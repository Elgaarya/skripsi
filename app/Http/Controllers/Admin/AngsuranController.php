<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Angsuran;
use App\Models\Pinjaman;
use Auth;
use Carbon\Carbon;
use Alert;
class AngsuranController extends Controller
{
    public function bayar($id)
    {
    	$angsuran = Angsuran::find($id);
    	if (Angsuran::where('pinjaman_id', $angsuran->pinjaman_id)->orderBy('angsuran_ke', 'asc')->first()->sudah_dibayar == 1) {
			$pinjaman = Angsuran::where('pinjaman_id', $angsuran->pinjaman_id)->where('sudah_dibayar', true)->orderBy('angsuran_ke', 'desc')->first();
	    	$angsuran->sudah_dibayar = true;
	    	$angsuran->sisa_piutang = $pinjaman->sisa_piutang - $angsuran->jumlah_bayar;
	    	$angsuran->tanggal_bayar = Carbon::now();
	    	$angsuran->save();
			$pinjaman = Pinjaman::find($angsuran->pinjaman_id);
	    	$pinjaman->total_piutang-= $angsuran->jumlah_bayar;
	    	$pinjaman->save();
    	} else {
			$pinjaman = Pinjaman::find($angsuran->pinjaman_id);
	    	$angsuran->sudah_dibayar = true;
	    	$angsuran->sisa_piutang = $pinjaman->total_piutang - $angsuran->jumlah_bayar;
	    	$angsuran->tanggal_bayar = Carbon::now();
	    	$angsuran->save();
	    	$pinjaman->total_piutang-= $angsuran->jumlah_bayar;
	    	$pinjaman->save();
    	}

    	Alert::success('Angsuran berhasil dibayar');
    	return redirect()->route('crud.app/angsuran.index');

    }
}
