<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Pengajuan_pinjaman;
use App\Models\Pinjaman;
use App\Models\Jenis_pinjaman;
use App\Models\Angsuran;
use Alert;
class PengajuanController extends Controller
{
    //
    public function terima($id)
    {
    	$pengajuan = Pengajuan_pinjaman::find($id);
    	$pengajuan->status = 'terima';
    	$pengajuan->save();

        $jp = Jenis_pinjaman::find($pengajuan->jenis_pinjaman_id);
        $angsuran = $pengajuan->besar/$pengajuan->lama;
        $bunga = $jp->bunga*$angsuran;
        $jumlah = $angsuran+$bunga;

        $pinjaman = new Pinjaman;
        $pinjaman->tanggal = $pengajuan->tanggal; 
        $pinjaman->besar = $pengajuan->besar; 
        $pinjaman->lama = $pengajuan->lama; 
        $pinjaman->angsuran = $angsuran; 
        $pinjaman->bunga = $bunga; 
        $pinjaman->jumlah = $jumlah; 
        $pinjaman->total_piutang = $jumlah*$pengajuan->lama; 
        $pinjaman->data_anggota_id = $pengajuan->data_anggota_id; 
        $pinjaman->jenis_pinjaman_id = $pengajuan->jenis_pinjaman_id;
        $pinjaman->save();

        for ($i=0; $i < $pinjaman->lama; $i++) { 
            $angsuran = new Angsuran;
            $angsuran->angsuran_ke = $i+1;
            $angsuran->jumlah_bayar = $jumlah;
            $angsuran->pinjaman_id = $pinjaman->id;
            $angsuran->data_anggota_id = $pinjaman->data_anggota_id;
            $angsuran->save();
        }

    	return redirect()->back();
    }

    public function tolak($id)
    {
   		$pengajuan = Pengajuan_pinjaman::find($id);
    	$pengajuan->status = 'tolak';
    	$pengajuan->save();


    	return redirect()->back();	

    }
}
