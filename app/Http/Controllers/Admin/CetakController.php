<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;
use App\Models\Pengajuan_pinjaman;

class CetakController extends Controller
{
    public function pengajuan($id)
    {
    	$data['p'] = Pengajuan_pinjaman::find($id);
    	$pdf = PDF::loadView('pdf.nisa', $data);
    	return $pdf->stream('pengajuan.pdf');
    }
}
