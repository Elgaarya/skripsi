<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\Data_anggotaRequest as StoreRequest;
use App\Http\Requests\Data_anggotaRequest as UpdateRequest;
// use App\Models\SimpananPokok;
// use App\Models\Jenis_simpanan;
use Auth;

class Data_anggotaCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\DataAnggota');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/data_anggota');
        $this->crud->setEntityNameStrings('data_anggota', 'data_anggotas');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $this->crud->setFromDb();

        // ------ CRUD FIELDS
        $this->crud->addField([
            'name' => 'bagian',
            'label' => 'Bagian',
            'type' => 'enum'
        ]);
        // $this->crud->addField($options, 'update/create/both');
        $this->crud->removeField('jenis_kelamin', 'update/create/both');
        $this->crud->addField([
            'name' => 'jenis_kelamin',
            'label' => 'Jenis Kelamin',
            'type' => 'enum'
        ]);
        $this->crud->removeField('status', 'update/create/both');
        $this->crud->addField([
            'name' => 'status',
            'label' => 'status',
            'type' => 'enum'
        ]);

        $this->crud->removeField('alamat', 'update/create/both');
        $this->crud->addField([
            'name' => 'alamat',
            'label' => 'Alamat',
            'type' => 'text'
        ]);

        $this->crud->removeField('document', 'update/create/both');

        $this->crud->addField([
            'name' => 'document',
            'label' => 'Image',
            'type' => 'upload',
            'upload' => true,
            'disk' => 'public', // if you store files in the /public folder, please ommit this; if you store them in /storage or S3, please specify it;
            // optional:'
        ]);
        $this->crud->addColumn([
            'name' => 'document', // The db column name
            'label' => "Profile image", // Table column heading
            'type' => 'image',
      
            // OPTIONALS
            // 'prefix' => 'folder/subfolder/',
            // image from a different disk (like s3 bucket)
            // 'disk' => 'disk-name', 
      
            // optional width/height if 25px is not ok with you
            // 'height' => '30px',
            // 'width' => '30px',
          ]);
        // $this->crud->addFields($array_of_arrays, 'update/create/both');
         // $this->crud->removeField('alamat', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');

        // ------ CRUD COLUMNS
         // add a
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']); // adjusts the properties of the passed in column (by name)
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);

        // ------ CRUD BUTTONS
        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
        // $this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function
        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->addButtonFromView($stack, $name, $view, $position); // add a button whose HTML is in a view placed at resources\views\vendor\backpack\crud\buttons
        // $this->crud->removeButton($name);
        // $this->crud->removeButtonFromStack($name, $stack);
        // $this->crud->removeAllButtons();
        // $this->crud->removeAllButtonsFromStack('line');

        // ------ CRUD ACCESS
        // $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);
        // $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);

        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        // $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ REVISIONS
        // You also need to use \Venturecraft\Revisionable\RevisionableTrait;
        // Please check out: https://laravel-backpack.readme.io/docs/crud#revisions
        // $this->crud->allowAccess('revisions');

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though:
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        // $this->crud->enableAjaxTable();

        // ------ DATATABLE EXPORT BUTTONS
        // Show export to PDF, CSV, XLS and Print buttons on the table view.
        // Does not work well with AJAX datatables.
        $this->crud->enableExportButtons();

        // ------ ADVANCED QUERIES
        
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '==', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->addClause('withoutGlobalScopes');
        // $this->crud->addClause('withoutGlobalScope', VisibleScope::class);
        // $this->crud->with(); // eager load relationships
        // $this->crud->orderBy();
        // $this->crud->groupBy();
        // $this->crud->limit();
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        // $jenis = Jenis_simpanan::find(2);

        // $sp = new SimpananPokok;
        // $sp->jumlah = $jenis->besar;
        // $sp->data_anggota_id = $this->crud->entry->id;
        // $sp->save();
        
        return redirect()->route('crud.app/data_anggota.index')->with('success', 'Data anggota berhasil ditambahkan.');
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return redirect()->route('crud.app/data_anggota.index')->with('success', 'Data anggota berhasil diupdate.');
    }
}
