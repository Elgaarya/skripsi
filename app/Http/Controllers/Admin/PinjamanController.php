<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;
use Carbon\Carbon;
use App\Models\Pinjaman;
use Charts;
use DB;

class PinjamanController extends Controller
{
    public function postInput(Request $request)
    {
    	$start = Carbon::parse($request->start);
    	$end = Carbon::parse($request->end)->addDay()->subMinute();
			$p = Pinjaman::whereBetween('created_at', [$start, $end])->get();

			$values = [];
			$labels = [];
			// dd($p);
			foreach($p as $row){
				array_push($values, $row->besar);
				array_push($labels, $row->data_anggota->nama);
			}

			$chart = Charts::database(Pinjaman::all(), 'bar', 'highcharts')
					->title('Besar Jumlah Pinjaman Anggota')
					->elementLabel('Jumlah Pembayaran')
					->responsive(false)
					->dimensions(1000, 500)
			->width(0)
			->values($values)
			->labels($labels);
    	return view('pinjaman.input', compact('p','chart'));
    }
    public function input(Request $request)
    {
    	return view('pinjaman.input');
    }
    public function cetak(Request $request)
    {
    	$start = Carbon::parse($request->start);
    	$end = Carbon::parse($request->end)->addDay()->subMinute();
    	$p = Pinjaman::whereBetween('created_at', [$start, $end])->get();

    	$data['p'] = $p;
    	$pdf = PDF::loadView('pdf.pinjaman', $data);
    	return $pdf->stream('pinjaman.pdf');
    }
}
