<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Simpanan_wajib;
use App\Models\Jenis_simpanan;
use Alert;
use App\Models\DataAnggota;
class SimpananWajibController extends Controller
{
    public function massCreate($month)
    {

    	$all = DataAnggota::all();
    	foreach ($all as $user) {
    		$simpanan = new Simpanan_wajib;
    		$simpanan->tanggal = date('o')."/".$month."/".date('d');
    		$simpanan->jumlah = Jenis_simpanan::find(1)->besar;
    		if ($user->simpananWajib()->count() == 0) {
    		    $simpanan->total = Jenis_simpanan::find(1)->besar;
    		} else {
    		    $last = $user->simpananWajib->last();
    		    $simpanan->total = $last->total+Jenis_simpanan::find(1)->besar;
    		}
    		$simpanan->data_anggota_id = $user->id;
    		$simpanan->save();
    	}

    	Alert::success('Berhasil menambahkan Simpanan Wajib')->flash();
    	return redirect()->route('crud.app/simpanan_wajib.index');
    }
}
