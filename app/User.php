<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;
use App\Models\DataAnggota;

class User extends Authenticatable
{
    use LaratrustUserTrait;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = ucwords($value);
    }

    public function simpananPokok()
    {
        return $this->hasOne(SimpananPokok::class);
    }
    public function simpananWajib()
    {
        return $this->hasMany(SimpananWajib::class);
    }
    public function simpananSukarela()
    {
        return $this->hasMany(SimpananSukarela::class);
    }
    public function data_anggota()
    {
        return $this->belongsTo(DataAnggota::class);
    }
}
