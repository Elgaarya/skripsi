<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use App\Role;
class User extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'users';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['data_anggota_id','email', 'password', 'roleId'];
    protected $hidden = ['password'];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
// 'name'
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = ucwords($value);
    }

    public function simpananPokok()
    {
        return $this->hasOne(SimpananPokok::class);
    }

    public function dataAnggota()
    {
        return $this->hasOne(DataAnggota::class);
    }
    public function pinjaman()
    {
        return $this->hasMany(pinjaman::class);
    }
    public function angsuran()
    {
        return $this->hasMany(angsuran::class);
    }
    public function pengajuan_pinjaman()
    {
        return $this->hasMany(pengajuan_pinjaman::class);
    }

    public function roles()
    {
        return $this->belongsTo(Role::class);
    }

    public function data_anggota()
    {
        return $this->belongsTo(DataAnggota::class);
    }
   
}
