<table class="table table-hover">
	<thead>
		<th>No Anggota</th>
		<th>Nama Anggota</th>
		<th>Jenis Pinjaman</th>
		<th>Besar (Rp)</th>
		<th>Angsuran (Rp)</th>
		<th>Bunga (Rp)</th>
		<th>Total Angsuran (Rp)</th>
	</thead>
	<tbody>
		<?php $__currentLoopData = $p; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $q): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<tr>
				<td><?php echo e($q->data_anggota->no_anggota); ?></td>
				<td><?php echo e($q->data_anggota->nama); ?></td>
				<td><?php echo e($q->jenis_pinjaman->nama); ?></td>
				<td><?php echo e(number_format($q->besar)); ?></td>
				<td><?php echo e(number_format($q->angsuran)); ?></td>
				<td><?php echo e(number_format($q->bunga)); ?></td>
				<td><?php echo e(number_format($q->jumlah)); ?></td>
			</tr>
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	</tbody>
</table>