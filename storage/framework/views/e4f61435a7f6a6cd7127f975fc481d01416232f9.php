<?php
	$i = 1;
	$current = 0;
?>
<table class="table table-hover">
	<thead>
		<th>No</th>
		<th>Tanggal</th>
		<th>Nama Anggota</th>
		<th>Besar Pinjaman</th>
		<th>Total Piutang</th>
		<th>Total Cicilan</th>
		<th>Sisa Piutang</th>
	</thead>
	<tbody>
		<?php $__currentLoopData = $object; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $single): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<?php if($current != $single->pinjaman_id): ?>
			<tr>
				<td><?php echo e($i); ?></td>
				<td><?php echo e($single->tanggal_bayar); ?></td>
				<td><?php echo e($single->data_anggota->nama); ?></td>
				<td><?php echo e(number_format($single->pinjaman->besar)); ?></td>
				<?php
					$piutang = $single->pinjaman->jumlah * $single->pinjaman->lama;
					$sisa = ($single->pinjaman->jumlah * $single->pinjaman->lama) - ($single->pinjaman->jumlah * $single->angsuran_ke);
					$cicilan = ($single->pinjaman->jumlah * $single->angsuran_ke)
				?>
				<td><?php echo e($single->tanggal_bayar == null ? '-' : number_format($piutang)); ?></td>
				<td><?php echo e($single->tanggal_bayar == null ? '-' : number_format($cicilan)); ?></td>
				<td><?php echo e($single->tanggal_bayar == null ? '-' : number_format($sisa)); ?></td>
			</tr>
		<?php endif; ?>
		<?php
			$i += 1;
			$current = $single->pinjaman_id;
		?>
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	</tbody>
</table>