<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-lg-4 col-sm-12">
            <form class="card auth_form" method="POST" action="<?php echo e(route('login')); ?>">
                <?php echo csrf_field(); ?>
                <div class="header">
                    <img class="logo" src="<?php echo e(asset('vendor/assets/images/logobpr.png')); ?>" alt="">
                    <h5>Log in</h5>
                </div>
                <div class="body">
                    <div class="input-group mb-3">
                        <input id="email" type="email" class="form-control" <?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" name="email" value="<?php echo e(old('email')); ?>" required autofocus placeholder="E-mail">
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="zmdi zmdi-account-circle"></i></span>
                        </div>
                        <?php if($errors->has('email')): ?>
                            <span class="alert-danger">
                                <small><?php echo e($errors->first('email')); ?></small>
                            </span>
                        <?php endif; ?>
                    </div>
                    <div class="input-group mb-3">
                        <input id="password" type="password" class="form-control<?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" name="password" required placeholder="Password">
                        <div class="input-group-append">                                
                            <span class="input-group-text"><i class="zmdi zmdi-lock"></i></span>
                        </div>  
                        <?php if($errors->has('password')): ?>
                            <span class="alert-danger">
                                <small><?php echo e($errors->first('password')); ?></small>
                            </span>
                        <?php endif; ?>                          
                    </div>
                    <div class="checkbox">
                        <input id="remember_me" type="checkbox">
                        <label for="remember_me">Remember Me</label>
                    </div>
                    <div class="form-group row mb-0">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary btn-block waves-effect waves-light">
                                <?php echo e(__('Login')); ?>

                            </button>

                            
                        </div>
                    </div>
                </div>
            </form>
            <div class="copyright text-center">
                &copy;
                <script>document.write(new Date().getFullYear())</script>,
                <span> Putu Elga Arya Putra
            </div>
        </div>
        <div class="col-lg-8 col-sm-12">
            <div class="card">
                <img src="<?php echo e(asset('vendor/assets/images/signup.svg')); ?>" alt="Sign In"/>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.login', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>