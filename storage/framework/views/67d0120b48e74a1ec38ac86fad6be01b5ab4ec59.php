<?php if(!$entry->sudah_dibayar == true): ?>
<form action="<?php echo e(route('angsuran.bayar',$entry->id)); ?>" method="post">
	<?php echo e(csrf_field()); ?>

	<?php echo e(method_field('PUT')); ?>

	<button class="btn btn-primary btn-xs" type="submit">Sudah diBayar</button>
</form>
<?php endif; ?>