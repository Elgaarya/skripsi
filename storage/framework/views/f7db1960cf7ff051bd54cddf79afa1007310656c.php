<html>
<head>
<title>Laporan Pinjaman Anggota</title>
<style type="text/css">
  body{
    font-size: 12px;
  }
  #page-wrap {
    width: 700px;
    margin: 0 auto;
  }
  .center-justified {
    text-align: justify;
    margin: 0 auto;
    width: 30em;
  }
  table.outline-table {
    border: 1px solid;
    border-spacing: 0;
  }
  tr.border-bottom td, td.border-bottom {
    border-bottom: 1px solid;
  }
  tr.border-top td, td.border-top {
    border-top: 1px solid;
  }
  tr.border-right td, td.border-right {
    border-right: 1px solid;
  }
  tr.border-right td:last-child {
    border-right: 0px;
  }
  tr.center td, td.center {
    text-align: center;
    vertical-align: text-top;
  }
  td.pad-left {
    padding-left: 5px;
  }
  td.pad-right {
    padding-right: 5px;
  }
  tr.right-center td, td.right-center {
    text-align: right;
    padding-right: 50px;
  }
  tr.right td, td.right {
    text-align: right;
  }
  .grey {
    background:grey;
  }
</style>
</head>
<body>
  <div id="page-wrap">
    <table width="100%">
      <tr class="center">
        <td><strong>Laporan Pinjaman Anggota</strong></td>
      </tr>
      <tr class="center">
        <td><strong>BPR BANK SUTRA MAS</strong></td>
      </tr>
      <tr class="center">
        <td><strong>Tahun <?php echo e(date('Y')); ?></strong></td>
      </tr>
    </table>
    &nbsp;
    <table width="100%" class="outline-table">
      <thead>
        <tr class="border-right border-bottom center">
          <td><strong>No Anggota (Rp)</strong></td>
          <td><strong>Nama Anggota (Rp)</strong></td>
          <td><strong>Jenis Pinjaman (Rp)</strong></td>
          <td><strong>Besar (Rp)</strong></td>
          <td><strong>Angsuran (Rp)</strong></td>
          <td><strong>Bunga (Rp)</strong></td>
          <td><strong>Total Angsuran (Rp)</strong></td>
        </tr>
      </thead>
      <tbody>
        <?php $__currentLoopData = $p; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $q): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr class="border-right">
          <td><?php echo e($q->data_anggota->no_anggota); ?></td>
          <td><?php echo e($q->data_anggota->nama); ?></td>
          <td><?php echo e($q->jenis_pinjaman->nama); ?></td>
          <td class="right"><?php echo e(number_format($q->besar)); ?></td>
          <td class="right"><?php echo e(number_format($q->angsuran)); ?></td>
          <td class="right"><?php echo e(number_format($q->bunga)); ?></td>
          <td class="right"><?php echo e(number_format($q->jumlah)); ?></td>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <tr class="border-right border-top center">
          <td colspan="3">Total Jumlah</td>
          <td class="right"><?php echo e(number_format($p->sum('besar'))); ?></td>
          <td class="right"><?php echo e(number_format($p->sum('angsuran'))); ?></td>
          <td class="right"><?php echo e(number_format($p->sum('bunga'))); ?></td>
          <td class="right"><?php echo e(number_format($p->sum('jumlah'))); ?></td>
        </tr>
      </tbody>
    </table>
    &nbsp;
    <table width="100%">
      <tr>
        <td colspan="3" class="right">Tabanan Bali, <?php echo e(date('d F Y')); ?></td>
      </tr>
      <tr><td colspan="3">&nbsp;</td></tr>
      <tr class="center"><td colspan="3">Pengurus</td></tr>
      <tr class="center">
        <td>Ketua</td>
        <td>&nbsp;</td>
        <td>Bendahara</td>
      </tr>
      <tr><td height="70px" colspan="3">&nbsp;</td></tr>
      <tr class="center">
        <td>(.................................)</td>
        <td>&nbsp;</td>
        <td>(.................................)</td>
      </tr>
    </table>
  </div>
</body>
</html>