<div class="row">
    <div class="col-md-12">
      <div class="box box-default">
          <div class="box-body">
            <p class="lead">Welcome <?php echo e(Auth::user()->data_anggota->nama); ?></p>
            <p>Selamat Datang di Sistem Informasi Peminjaman Modal!</p>
            <p>Anda login menggunakan akun <strong><?php echo e(Auth::user()->email); ?></strong></p>
          </div>
      </div>
    </div>
    
</div>