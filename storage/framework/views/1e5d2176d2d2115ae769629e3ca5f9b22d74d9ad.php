<?php if(Auth::user()->hasRole('admin')): ?>
	Total piutang : <a class="btn btn-success"><?php echo e("Rp. " . number_format(App\Models\Pinjaman::sum('total_piutang'))); ?></a>
<?php endif; ?>