<div class="navbar-brand">
    <button class="btn-menu ls-toggle-btn" type="button"><i class="zmdi zmdi-menu"></i></button>
    <a href="index.html"><img src="<?php echo e(asset('vendor/assets/images/logobpr.png')); ?>" width="25" alt="Aero"><span class="m-l-10">Dashboard</span></a>
</div>
<div class="menu">
    <ul class="list">
        <li>
            <div class="user-info">
                <a class="image" href="<?php echo e(route('backpack.account.info')); ?>"><img src="<?php echo e(backpack_avatar_url(Auth::user())); ?>" alt="User"></a>
                <div class="detail">
                    <span><?php echo e(Auth::user()->data_anggota->nama); ?></span>
                    <small><?php echo e(Auth::user()->data_anggota->bagian); ?></small>                        
                </div>
            </div>
        </li>
        <?php if(Auth::user()->roleId == 1): ?>
        <li class="active open"><a href="<?php echo e(backpack_url('dashboard')); ?>"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a></li>
        <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-account"></i><span>Master Anggota</span></a>
            <ul class="ml-menu">
                <li><a href="<?php echo e(backpack_url('data_anggota')); ?>">Data Anggota</a></li>
                <li><a href="<?php echo e(route('crud.app/user.index')); ?>">Data Pengguna</a></li>                
            </ul>
        </li>
        <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-money"></i><span>Master Transaksi</span></a>
            <ul class="ml-menu">
                <li><a href="<?php echo e(backpack_url('jenis_pinjaman')); ?>">Jenis Pinjaman</a></li>
                <li><a href="<?php echo e(backpack_url('pinjaman')); ?>">Pinjaman</a></li>
                <li><a href="<?php echo e(backpack_url('angsuran')); ?>">Angsuran</a></li>
                <li><a href="<?php echo e(route('crud.app/pengajuan_pinjaman.index')); ?>"><i class="fa fa-credit-card-alt"></i>
                    <?php echo e("Pengajuan Pinjaman"); ?>

                    <?php if(Auth::user()->hasRole('admin')): ?>
                      <span class="badge pull-right"><?php echo e(App\pengajuan_pinjaman::where('status','pending')->count()); ?></span>
                    <?php endif; ?>
                    </a>
                </li>
            </ul>
        </li>
        <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-assignment"></i><span>Laporan</span></a>
            <ul class="ml-menu">
                <li><a href="<?php echo e(route('pinjaman.input')); ?>">Laporan Pinjaman</a></li>
                <li><a href="<?php echo e(route('report.get.angsuran')); ?>">Laporan Angsuran</a></li>
            </ul>
        </li>
        <li class=""><a href="<?php echo e(route('backpack.auth.logout')); ?>"><i class="zmdi zmdi-power"></i><span>Logout</span></a></li>
        <?php endif; ?>
        <?php if(Auth::user()->roleId == 4): ?>
            <li class="active open"><a href="<?php echo e(backpack_url('dashboard')); ?>"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a></li>
            <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-money"></i><span>Master Transaksi</span></a>
                <ul class="ml-menu">
                    <li><a href="<?php echo e(backpack_url('pinjaman')); ?>">Pinjaman</a></li>
                    <li><a href="<?php echo e(backpack_url('angsuran')); ?>">Angsuran</a></li>
                    <li><a href="<?php echo e(backpack_url('pengajuan_pinjaman')); ?>">Pengajuan Pinjaman</a></li>
                </ul>
            </li>
            <li class=""><a href="<?php echo e(route('backpack.auth.logout')); ?>"><i class="zmdi zmdi-power"></i><span>Logout</span></a></li>
        <?php endif; ?>
        <?php if(Auth::user()->roleId == 2): ?>
            <li class="active open"><a href="<?php echo e(backpack_url('dashboard')); ?>"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a></li>
            <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-assignment"></i><span>Laporan</span></a>
                <ul class="ml-menu">
                    <li><a href="<?php echo e(route('pinjaman.input')); ?>">Laporan Pinjaman</a></li>
                    <li><a href="<?php echo e(route('report.get.angsuran')); ?>">Laporan Angsuran</a></li>
                </ul>
            </li>
            <li class=""><a href="<?php echo e(route('backpack.auth.logout')); ?>"><i class="zmdi zmdi-power"></i><span>Logout</span></a></li>
        <?php endif; ?>
        <?php if(Auth::user()->roleId == 3): ?>
            <li class=""><a href="<?php echo e(backpack_url('dashboard')); ?>"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a></li>
            <li class=""><a href="<?php echo e(backpack_url('pinjaman')); ?>"><i class="zmdi zmdi-money"></i><span>Pinjaman</span></a></li>
            <li class=""><a href="<?php echo e(backpack_url('angsuran')); ?>"><i class="zmdi zmdi-book"></i><span>Angsuran</span></a></li>
            <li class=""><a href="<?php echo e(backpack_url('pengajuan_pinjaman')); ?>"><i class="zmdi zmdi-card"></i><span>Pengajuan Pinjaman</span></a></li>
            <li class=""><a href="<?php echo e(route('backpack.auth.logout')); ?>"><i class="zmdi zmdi-power"></i><span>Logout</span></a></li>
        <?php endif; ?>
        
        
        
    </ul>
</div>