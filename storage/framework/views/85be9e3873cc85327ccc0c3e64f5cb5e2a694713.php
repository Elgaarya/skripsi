<?php $__env->startSection('content'); ?>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="box box-default">
                <div class="body box-body">
                    <h2><center>Laporan Angsuran</center></h2>
                    <form class="form" action="<?php echo e(route('report.post.angsuran')); ?>" method="post">
                        <?php echo e(csrf_field()); ?>

                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tanggal Awal</label>
                                        <input type="date" name ="start" class="form-control" value="<?php echo e(app('request')->input('start')); ?>" placeholder="col-md-6">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tanggal Akhir</label>
                                        <input type="date" name="end" class="form-control" value="<?php echo e(app('request')->input('end')); ?>" placeholder="col-md-6">
                                        <input type="hidden" name="pdf" value="1">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <button type="submit" class="btn col-md-3 btn-success"><?php echo e("Submit"); ?></button>
                            </div>
                        </div>
                    </form>
                    <?php if(app('request')->input('pdf')): ?>
                    <form class="form" action="<?php echo e(route('report.pdf.angsuran')); ?>" method="post">
                        <div class="col-md-6">
                            <?php echo e(csrf_field()); ?>

                            <input type="hidden" name="start" class="form-control" value="<?php echo e(app('request')->input('start')); ?>">
                            <input type="hidden" name="end" class="form-control" value="<?php echo e(app('request')->input('end')); ?>">
                            <button type="submit" class="btn btn-info col-md-3"><?php echo e("Cetak"); ?></button>
                        </div>
                        <div class="col-md-12">
                            <?php echo $__env->make('reports.tables.angsuran', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        </div>   
                        <?php echo $chart->render(); ?>


                    </form>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backpack::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>