<html>
<head>
<title>Formulir Pengajuan Pinjaman</title>
<style type="text/css">
  #page-wrap {
    width: 700px;
    margin: 0 auto;
  }
  .center-justified {
    text-align: justify;
    margin: 0 auto;
    width: 30em;
  }
  table.outline-table {
    border: 1px solid;
    border-spacing: 0;
  }
  tr.border-bottom td, td.border-bottom {
    border-bottom: 1px solid;
  }
  tr.border-top td, td.border-top {
    border-top: 1px solid;
  }
  tr.border-right td, td.border-right {
    border-right: 1px solid;
  }
  tr.border-right td:last-child {
    border-right: 0px;
  }
  tr.center td, td.center {
    text-align: center;
    vertical-align: text-top;
  }
  td.pad-left {
    padding-left: 5px;
  }
  td.pad-right {
    padding-right: 5px;
  }
  tr.right-center td, td.right-center {
    text-align: right;
    padding-right: 50px;
  }
  tr.right td, td.right {
    text-align: right;
  }
  .grey {
    background:grey;
  }
</style>
</head>
<body>
  <div id="page-wrap">
    <table width="100%">
      <tbody>
        <tr class="center">
          <td>
            <strong>BPR BUNGA SUTRA MAS</strong><br>
            <small>FORMULIR PENGAJUAN PEMBIAYAAN</small>
          </td>
        </tr>
      </tbody>
    </table>
    <hr>
    <table>
      <tbody>
        <tr>
          <td width="100%" colspan="2"><small>Dengan ini saya : </small></td>
        </tr>
        <tr>
          <td colspan="2" style="padding-left: 50px"><small>Nama/NIP : <strong><?php echo e($p->data_anggota->nama); ?></strong></small></td>
        </tr>
        <tr>
          <td style="padding-left: 50px"><small>Bidang/Bagian/Unit : </small></td>
          <td><small><strong><?php echo e($p->data_anggota->bagian); ?></strong></small></td>
        </tr>
      </tbody>
    </table>
    &nbsp;
    <table width="100%">
      <tr><td colspan="7"><small>Mengajukan pembiayaan berupa pembelian/penyewaan : </small></td></tr>
      <tr><td colspan="7">&nbsp;</td></tr>
      <tr>
        <td>&nbsp;</td>
        <td><small><strong><?php echo e($p->jenis_pinjaman->nama); ?></strong></small></td>
        <td class="right"><small>sebesar</small></td>
        <td class="right"><small>Rp. <strong><?php echo e(number_format($p->besar)); ?></strong></small></td>
        <td class="right"><small>dicicil</small></td>
        <td class="right"><small><strong><?php echo e($p->lama); ?></strong></small></td>
        <td class="right"><small>kali</small></td>
      </tr>
      <tr><td colspan="7">&nbsp;</td></tr>
      <tr><td colspan="7"><small>Dan setuju memberikan jasa pengelolaan ke BPR BUNGA SUTRA MAS sebesar 1% untuk pembelian alat rumah tangga dan 1% untuk pembiayaan point 2 di atas.</small></td></tr>
      <tr><td class="right" colspan="7"><small>Tabanan Bali, <?php echo e(date('d F Y')); ?></small></td></tr>
    </table>
    <table width="100%">
      <tr class="center">
        <td width="50%"><small>Menyetujui</small></td>
        <td width="50%"><small>yang mengajukan</small></td>
      </tr>
      <tr class="center">
        <td width="50%"><small>Kepala Pinjaman</small></td>
        <td width="50%">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" height="50px">&nbsp;</td>
      </tr>
      <tr class="center">
        <td width="50%"><small>Kadek Agus Kurniawan Putra, ST</small></td>
        <td width="50%">(....................................................)</td>
      </tr>
    </table>
    <hr>
  </div>
  <div id="page-wrap">
    <table width="100%">
      <tbody>
        <tr class="center">
          <td>
            <strong>BPR BUNGA SUTRA MAS</strong><br>
            <small>FORMULIR PENGAJUAN PEMBIAYAAN</small>
          </td>
        </tr>
      </tbody>
    </table>
    <hr>
    <table>
      <tbody>
        <tr>
          <td width="100%" colspan="2"><small>Yang bertanda tangan di bawah ini saya : </small></td>
        </tr>
        <tr>
          <td colspan="2" style="padding-left: 50px"><small>Nama : Kadek Agus Kurniawan Putra, ST</small></td>
        </tr>
        <tr>
          <td width="100%" colspan="2"><small>Selaku kepala pinjaman, dengan ini mewakilkan kepada anggota : </small></td>
        </tr>
        <tr>
          <td colspan="2" style="padding-left: 50px"><small>Nama : <strong><?php echo e($p->data_anggota->nama); ?></strong></small></td>
        </tr>
      </tbody>
    </table>
    &nbsp;
    <table width="100%">
      <tr><td colspan="7"><small>Untuk bertindak atas nama BPR BUNGA SUTRA MAS untuk membeli barang atau membayar jasa guna memenuhi pembiayaan/pembelian/penyewaan : </small></td></tr>
      <tr><td colspan="7">&nbsp;</td></tr>
      <tr>
        <td>&nbsp;</td>
        <td><small><strong><?php echo e($p->jenis_pinjaman->nama); ?></strong></small></td>
        <td class="right"><small>sebesar</small></td>
        <td class="right"><small>Rp. <strong><?php echo e(number_format($p->besar)); ?></strong></small></td>
        <td class="right"><small>dicicil</small></td>
        <td class="right"><small><strong><?php echo e($p->lama); ?></strong></small></td>
        <td class="right"><small>kali</small></td>
      </tr>
      <tr><td colspan="7">&nbsp;</td></tr>
      <tr><td class="right" colspan="7"><small>Tangerang Selatan, <?php echo e(date('d F Y')); ?></small></td></tr>
    </table>
    <table width="100%">
      <tr class="center">
        <td width="50%"><small>Yang bertindak atas nama BPR</small></td>
        <td width="50%"><small>Kepala Pinjaman</small></td>
      </tr>
      <tr class="center">
        <td width="50%"><small>(.........................)</small></td>
        <td width="50%">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" height="70px">&nbsp;</td>
      </tr>
      <tr class="center">
        <td width="50%"><small>(....................................................)</small></td>
        <td width="50%"><small>(Kadek Agus Kurniawan Putra, ST)</small></td>
      </tr>
    </table>
    <p><sup>*)</sup> Tandai sesuai pengajuan</p>
    <hr>
  </div>
  <div id="page-wrap">
    <hr>
    <table width="30%" class="outline-table center right" style="position: absolute; right: 10">
      <tr>
        <td class="center">
          <small>BUKTI PEMBAYARAN</small>
        </td>
      </tr>
    </table>
    <table width="100%" style="margin-top: 20px; padding-left: 70px">
      <tr><td><small>BPR BUNGA SUTRA MAS</small></td></tr>
    </table>
    &nbsp;
    <table width="100%">
      <tr>
        <td width="20%"><small>Telah terima dari </small></td>
        <td><small>: Bendahara BPR BUNGA SUTRA MAS</small></td>
      </tr>
      <tr>
        <td width="20%"><small>Uang sejumlah </small></td>
        <td><small>: Rp. <strong><?php echo e(number_format($p->besar)); ?></strong></small></td>
      </tr>
      <tr>
        <td width="20%"><small>&nbsp;</small></td>
        <td><small> : (.........................................................................................................................)</small></td>
      </tr>
    </table>

    <table width="100%">
      <tr>
        <td width="20%"><small>Untuk Pembayaran</small></td>
        <td colspan="2"><small>: Pengajuan pembiayaan</small></td>
      </tr>
      <tr>
        <td width="20%">&nbsp;</td>
        <td width="10%">&nbsp;</td>
        <td><small><strong><?php echo e($p->jenis_pinjaman->nama); ?></strong></small></td>
      </tr>
      <tr>
        <td colspan="3">&nbsp;</td>
      </tr>
      <tr class="right">
        <td colspan="3"><small>Tabanan Bali, <?php echo e(date('d F Y')); ?></small></td>
      </tr>
    </table>
    &nbsp;
    <table width="100%">
      <tr class="center">
        <td width="50%"><small>Yang membayarkan</small></td>
        <td width="50%"><small>Penerima</small></td>
      </tr>
      <tr class="center">
        <td width="50%"><small>Bendahara BPR BUNGA SUTRA MAS</small></td>
        <td width="50%">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" height="100px">&nbsp;</td>
      </tr>
      <tr class="center">
        <td width="50%">(....................................................)</td>
        <td width="50%">(....................................................)</td>
      </tr>
    </table>
    <p><sup>*)</sup> Tandai sesuai pengajuan</p>
    <hr>
  </div>
</body>
</html>