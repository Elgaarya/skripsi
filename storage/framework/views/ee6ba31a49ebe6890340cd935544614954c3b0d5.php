<?php if($entry->status != 'terima' and $entry->status != 'tolak'): ?>
<form action="<?php echo e(route('pengajuan.tolak',$entry->id)); ?>" method="post">
	<?php echo e(csrf_field()); ?>

	<?php echo e(method_field('PUT')); ?>

	<button class="btn btn-primary center" type="submit">Tolak</button>
</form>
<?php endif; ?>