<?php if($entry->status != 'terima' and $entry->status != 'tolak'): ?>
<form action="<?php echo e(route('pengajuan.terima',$entry->id)); ?>" method="post">
	<?php echo e(csrf_field()); ?>

	<?php echo e(method_field('PUT')); ?>

	<br><button class="btn btn-primary pull-left" type="submit">Terima</button>
</form>
<?php endif; ?>
