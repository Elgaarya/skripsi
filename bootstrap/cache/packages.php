<?php return array (
  'backpack/base' => 
  array (
    'providers' => 
    array (
      0 => 'Backpack\\Base\\BaseServiceProvider',
    ),
  ),
  'backpack/crud' => 
  array (
    'providers' => 
    array (
      0 => 'Backpack\\CRUD\\CrudServiceProvider',
    ),
    'aliases' => 
    array (
      'CRUD' => 'Backpack\\CRUD\\CrudServiceProvider',
    ),
  ),
  'backpack/generators' => 
  array (
    'providers' => 
    array (
      0 => 'Backpack\\Generators\\GeneratorsServiceProvider',
    ),
  ),
  'barryvdh/laravel-dompdf' => 
  array (
    'providers' => 
    array (
      0 => 'Barryvdh\\DomPDF\\ServiceProvider',
    ),
    'aliases' => 
    array (
      'PDF' => 'Barryvdh\\DomPDF\\Facade',
    ),
  ),
  'barryvdh/laravel-elfinder' => 
  array (
    'providers' => 
    array (
      0 => 'Barryvdh\\Elfinder\\ElfinderServiceProvider',
    ),
  ),
  'consoletvs/charts' => 
  array (
    'providers' => 
    array (
      0 => 'ConsoleTVs\\Charts\\ChartsServiceProvider',
    ),
    'aliases' => 
    array (
      'Charts' => 'ConsoleTVs\\Charts\\Facades\\Charts',
    ),
  ),
  'consoletvs/support' => 
  array (
    'providers' => 
    array (
      0 => 'ConsoleTVs\\Support\\SupportServiceProvider',
    ),
  ),
  'creativeorange/gravatar' => 
  array (
    'providers' => 
    array (
      0 => 'Creativeorange\\Gravatar\\GravatarServiceProvider',
    ),
    'aliases' => 
    array (
      'Gravatar' => 'Creativeorange\\Gravatar\\Facades\\Gravatar',
    ),
  ),
  'fideloper/proxy' => 
  array (
    'providers' => 
    array (
      0 => 'Fideloper\\Proxy\\TrustedProxyServiceProvider',
    ),
  ),
  'intervention/image' => 
  array (
    'providers' => 
    array (
      0 => 'Intervention\\Image\\ImageServiceProvider',
    ),
    'aliases' => 
    array (
      'Image' => 'Intervention\\Image\\Facades\\Image',
    ),
  ),
  'jenssegers/date' => 
  array (
    'providers' => 
    array (
      0 => 'Jenssegers\\Date\\DateServiceProvider',
    ),
    'aliases' => 
    array (
      'Date' => 'Jenssegers\\Date\\Date',
    ),
  ),
  'laracasts/generators' => 
  array (
    'providers' => 
    array (
      0 => 'Laracasts\\Generators\\GeneratorsServiceProvider',
    ),
  ),
  'laravel/tinker' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Tinker\\TinkerServiceProvider',
    ),
  ),
  'nunomaduro/collision' => 
  array (
    'providers' => 
    array (
      0 => 'NunoMaduro\\Collision\\Adapters\\Laravel\\CollisionServiceProvider',
    ),
  ),
  'prologue/alerts' => 
  array (
    'providers' => 
    array (
      0 => 'Prologue\\Alerts\\AlertsServiceProvider',
    ),
    'aliases' => 
    array (
      'Alert' => 'Prologue\\Alerts\\Facades\\Alert',
    ),
  ),
  'santigarcor/laratrust' => 
  array (
    'providers' => 
    array (
      0 => 'Laratrust\\LaratrustServiceProvider',
    ),
    'aliases' => 
    array (
      'Laratrust' => 'Laratrust\\LaratrustFacade',
    ),
  ),
);