<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Models\Jenis_simpanan;
use App\Models\Jenis_pinjaman;
use App\Models\DataAnggota;
use App\Role;
use Illuminate\Support\Facades\Hash;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $admin = new Role;
        $admin->id = 1;
        $admin->name = "admin";
        $admin->display_name = 'Admin';
        $admin->description = '';
        $admin->save();

        $ketua = new Role;
        $ketua->id = 2;
        $ketua->name = "manager";
        $ketua->display_name = 'Manager';
        $ketua->description = '';
        $ketua->save();

        $anggota = new Role;
        $anggota->id = 3;
        $anggota->name = "anggota";
        $anggota->display_name = 'Anggota';
        $anggota->description = '';
        $anggota->save();

        $anggota = new Role;
        $anggota->id = 4;
        $anggota->name = "pegawai";
        $anggota->display_name = 'Pegawai';
        $anggota->description = '';
        $anggota->save();

        $data1 = new DataAnggota;
        $data1->no_anggota = '375.05.98';
        $data1->nip = '1522389397';
        $data1->nama = "Putu Elga Arya Putra";
        $data1->alamat = "Denpasar";
        $data1->bagian = "Administrasi";
        $data1->tempat_lahir = "Denpasar";
        $data1->tanggal_lahir ="1998-10-12";
        $data1->no_telp = "087869710305";
        $data1->status = "belum menikah" ;
        $data1->jenis_kelamin = "laki-laki";
        $data1->save();
        $user1 = new User;
        $user1->data_anggota_id = $data1->id;
        $user1->email = 'admin@admin.com';
        $user1->password = Hash::make('password');
        $user1->save();

        $data2 = new DataAnggota;
        $data2->no_anggota = '338.05.28';
        $data2->nip = '1234567890';
        $data2->nama = "Putu Eka Yanti";
        $data2->alamat = "Denpasar";
        $data2->bagian = "Manager";
        $data2->tempat_lahir = "Denpasar";
        $data2->tanggal_lahir ="1997-10-12";
        $data2->no_telp = "082319878833";
        $data2->status = "menikah" ;
        $data2->jenis_kelamin = "perempuan";
        $data2->save();
        $user2 = new User;
        $user2->data_anggota_id = $data2->id;
        $user2->email = 'manager@gmail.com';
        $user2->password = Hash::make('password');
        $user2->save();

        $data3 = new DataAnggota;
        $data3->no_anggota = '323.98.62';
        $data3->nip = '1123457896';
        $data3->nama = "Customer";
        $data3->alamat = "Bali";
        $data3->bagian = "Anggota";
        $data3->tempat_lahir = "Denpasar";
        $data3->tanggal_lahir ="1997-10-12";
        $data3->no_telp = "082319878833";
        $data3->status = "menikah" ;
        $data3->jenis_kelamin = "laki-laki";
        $data3->save();
        $user3 = new User;
        $user3->data_anggota_id = $data3->id;
        $user3->email = 'customer@gmail.com';
        $user3->password = Hash::make('password');
        $user3->save();

        $data4 = new DataAnggota;
        $data4->no_anggota = "323.98.21";
        $data4->nip = "1123457822";
        $data4->nama = "Gede Riski";
        $data4->alamat = "Bali";
        $data4->bagian = "Pegawai";
        $data4->tempat_lahir = "Denpasar";
        $data4->tanggal_lahir ="1994-11-22";
        $data4->no_telp = "08231989281";
        $data4->status = "Belum Menikah" ;
        $data4->jenis_kelamin = "laki-laki";
        $data4->save();
        $user4 = new User;
        $user4->data_anggota_id = $data4->id;
        $user4->email = 'pegawai@gmail.com';
        $user4->password = Hash::make('password');
        $user4->save();

        $role1 = User::find($user1->id);
        $role1->Roles()->attach(Role::find(1));
        $role1->save();

        $role2 = User::find($user2->id);
        $role2->Roles()->attach(Role::find(2));
        $role2->save();

        $role3 = User::find($user3->id);
        $role3->Roles()->attach(Role::find(3));
        $role3->save();

        $role4 = User::find($user4->id);
        $role4->Roles()->attach(Role::find(4));
        $role4->save();

        $b = new Jenis_pinjaman;
        $b->id = 1;
        $b->kode = 'SP';
        $b->nama = 'Simpan Pinjam';
        $b->bunga = '0.01';
        $b->save();

        $b = new Jenis_pinjaman;
        $b->id = 2;
        $b->kode = 'PE';
        $b->nama = 'Pinjaman Emergency';
        $b->bunga = '0.01';
        $b->save();

        $b = new Jenis_pinjaman;
        $b->id = 3;
        $b->kode = 'PU';
        $b->nama = 'Pinjaman Umroh';
        $b->bunga = '0.005';
        $b->save();

        $b = new Jenis_pinjaman;
        $b->id = 4;
        $b->kode = 'PA';
        $b->nama = 'Pinjaman Alat Rumah Tangga';
        $b->bunga = '0.01';
        $b->save();
    }
}
