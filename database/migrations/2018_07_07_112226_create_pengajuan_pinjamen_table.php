<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengajuanPinjamenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengajuan_pinjamen', function (Blueprint $table) {
            $table->increments('id');
            $table->date('tanggal');
            $table->integer('besar');
            $table->integer('lama');
            $table->enum('status',['terima','tolak','pending'])->default('pending');
            $table->string('keterangan')->nullable();
            $table->timestamps();
        });
         Schema::table('pengajuan_pinjamen', function (Blueprint $table) {
            $table->unsignedInteger('data_anggota_id');
            $table->foreign('data_anggota_id')->references('id')->on('data_anggotas')->onDelete('cascade');
        });
         Schema::table('pengajuan_pinjamen', function (Blueprint $table) {
            $table->unsignedInteger('jenis_pinjaman_id');
            $table->foreign('jenis_pinjaman_id')->references('id')->on('jenis_pinjamen')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengajuan_pinjamen');
    }
}
