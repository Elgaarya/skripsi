<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataAnggotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_anggotas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_anggota',9);
            $table->string('nip',18);
            $table->string('nama',30);
            $table->enum('bagian', ['Administrasi','Manager','Anggota','Pegawai']);
            $table->string('alamat',100);
            $table->string('tempat_lahir',20);
            $table->date('tanggal_lahir');
            $table->string('no_telp',13);
            $table->enum('status', ['menikah','belum menikah']);
            $table->enum('jenis_kelamin', ['laki-laki','perempuan']);
            $table->timestamps();
        });
        
        Schema::table('pinjamen', function (Blueprint $table) {
            $table->unsignedInteger('data_anggota_id');
            $table->foreign('data_anggota_id')->references('id')->on('data_anggotas')->onUpdate('cascade');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedInteger('data_anggota_id');
            $table->foreign('data_anggota_id')->references('id')->on('data_anggotas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_anggotas');
    }
}
