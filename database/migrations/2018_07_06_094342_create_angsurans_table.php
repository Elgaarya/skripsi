<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAngsuransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('angsurans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('angsuran_ke');
            $table->boolean('sudah_dibayar')->default(0);
            $table->integer('jumlah_bayar');
            $table->integer('sisa_piutang')->nullable();
            $table->date('tanggal_bayar')->nullable();
            $table->timestamps();
        });
        Schema::table('angsurans', function (Blueprint $table) {
            $table->unsignedInteger('pinjaman_id');
            $table->foreign('pinjaman_id')->references('id')->on('pinjamen')->onUpdate('cascade');
        });
        Schema::table('angsurans', function (Blueprint $table) {
            $table->unsignedInteger('data_anggota_id');
            $table->foreign('data_anggota_id')->references('id')->on('data_anggotas')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('angsurans');
    }
}
