<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePinjamenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pinjamen', function (Blueprint $table) {
            $table->increments('id');
            $table->date('tanggal');
            $table->integer('besar');
            $table->integer('lama');
            $table->integer('angsuran')->nullable();
            $table->float('bunga', 15)->nullable();
            $table->integer('jumlah')->nullable();
            $table->integer('total_piutang')->nullable();
            $table->timestamps();
        });
        Schema::table('pinjamen', function (Blueprint $table) {
            $table->unsignedInteger('jenis_pinjaman_id');
            $table->foreign('jenis_pinjaman_id')->references('id')->on('jenis_pinjamen')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pinjamen');
    }
}
